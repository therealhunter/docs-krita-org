# Spanish translations for docs_krita_org_reference_manual___brushes___brush_engines___clone_engine.po package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Eloy Cuadra <ecuadra@eloihr.net>, 2019.
# Sofia Priego <spriego@darksylvania.net>, %Y.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___brushes___brush_engines___clone_engine\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-06-19 22:04+0100\n"
"Last-Translator: Sofia Priego <spriego@darksylvania.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../<generated>:1
msgid "Clone from all visible layers"
msgstr ""

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:1
#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:18
msgid "The Clone Brush Engine manual page."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:11
#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:16
msgid "Clone Brush Engine"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:11
msgid "Brush Engine"
msgstr "Motor de pinceles"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:11
msgid "Clone Tool"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:30
msgid ".. image:: images/icons/clonebrush.svg"
msgstr ".. image:: images/icons/clonebrush.svg"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:31
msgid ""
"The clone brush is a brush engine that allows you to paint with a "
"duplication of a section of a paint-layer. This is useful in manipulation of "
"photos and textures. You have to select a source and then you can paint to "
"copy or clone the source to a different area. Other applications normally "
"have a separate tool for this, Krita has a brush engine for this."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:34
msgid "Usage and Hotkeys"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:36
msgid ""
"To see the source, you need to set the brush-cursor settings to brush "
"outline."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:38
msgid ""
"The clone tool can now clone from the projection and it's possible to change "
"the clone source layer. Press the :kbd:`Ctrl + Alt +` |mouseleft| shortcut "
"to select a new clone source on the current layer. The :kbd:`Ctrl +` |"
"mouseleft| shortcut to select a new clone source point on the layer that was "
"active when you selected the clone op."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:42
msgid ""
"The :kbd:`Ctrl + Alt +` |mouseleft| shortcut is temporarily disabled on "
"2.9.7."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:45
msgid "Settings"
msgstr "Preferencias"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:47
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:48
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:49
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:52
msgid "Painting mode"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:54
msgid "Healing"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:55
msgid ""
"This turns the clone brush into a healing brush: often used for removing "
"blemishes in photo retouching, and maybe blemishes in painting."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:57
msgid "Only works when there's a perspective grid visible."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:59
msgid "Perspective correction"
msgstr "Corrección de la perspectiva"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:60
msgid "This feature is currently disabled."
msgstr "Esta función está desactivada en la actualidad."

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:61
msgid "Source Point move"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:62
msgid ""
"This will determine whether you will replicate the source point per dab or "
"per stroke. Can be useful when used with the healing brush."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:63
msgid "Source Point reset before a new stroke"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:64
msgid ""
"This will reset the source point everytime you make a new stroke. So if you "
"were cloning a part in one stroke, having this active will allow you to "
"clone the same part again in a single stroke, instead of using the source "
"point as a permanent offset."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:66
msgid ""
"Tick this to force cloning of all layers instead of just the active one."
msgstr ""
