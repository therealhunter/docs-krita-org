# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 11:18+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../general_concepts/projection.rst:None
msgid ".. image:: images/category_projection/projection-cube_09.svg"
msgstr ".. image:: images/category_projection/projection-cube_09.svg"

#: ../../general_concepts/projection.rst:1
msgid "The Perspective Projection Category."
msgstr "De category perspectivische projectie."

#: ../../general_concepts/projection.rst:15
msgid "Perspective Projection"
msgstr "Perspectivische projectie"

#: ../../general_concepts/projection.rst:17
msgid ""
"The Perspective Projection tutorial is one of the Kickstarter 2015 tutorial "
"rewards. It's about something that humanity has known scientifically for a "
"very long time, and decent formal training will teach you about this. But I "
"think there are very very few tutorials about it in regard to how to achieve "
"it in digital painting programs, let alone open source."
msgstr ""
"De inleiding Perspectief projectie is een van de Kickstarter 2015 "
"inleidingbeloningen. Het gaat overt iets dat de mensheid volgens de "
"wetenschap al erg lang weet en goede formele training u er over zal "
"onderwijzen. Maar ik denk dat er erg weinig inleidingen over zijn over hoe "
"het te gebruiken in digitale tekenprogramma's, laat staan open-source."

#: ../../general_concepts/projection.rst:19
msgid ""
"The tutorial is a bit image heavy, and technical, but I hope the skill it "
"teaches will be really useful to anyone trying to get a grasp on a "
"complicated pose. Enjoy, and don't forget to thank `Raghukamath <https://www."
"raghukamath.com/>`_ for choosing this topic!"
msgstr ""
"De inleiding bevat een beetje veel afbeeldingen en is technisch, maar ik "
"hoop dat de vaardigheid die het onderwijst erg nuttig voor iedereen zal zijn "
"om een gecompliceerde pose te bevatten. Geniet en vergeet niet `Raghukamath "
"<https://www.raghukamath.com/>`_ te danken om dit onderwerp te kiezen!"

#: ../../general_concepts/projection.rst:24
msgid "Parts:"
msgstr "Onderdelen:"
