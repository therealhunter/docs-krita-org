# Translation of docs_krita_org_general_concepts___projection___axonometric.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: general_concepts\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-25 14:53+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection-cube_07.svg"
msgstr ".. image:: images/category_projection/projection-cube_07.svg"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection-cube_08.svg"
msgstr ".. image:: images/category_projection/projection-cube_08.svg"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection-cube_09.svg"
msgstr ".. image:: images/category_projection/projection-cube_09.svg"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection-cube_10.svg"
msgstr ".. image:: images/category_projection/projection-cube_10.svg"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection-cube_11.svg"
msgstr ".. image:: images/category_projection/projection-cube_11.svg"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_15.png"
msgstr ".. image:: images/category_projection/projection_image_15.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_16.png"
msgstr ".. image:: images/category_projection/projection_image_16.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_17.png"
msgstr ".. image:: images/category_projection/projection_image_17.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_18.png"
msgstr ".. image:: images/category_projection/projection_image_18.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_19.png"
msgstr ".. image:: images/category_projection/projection_image_19.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_20.png"
msgstr ".. image:: images/category_projection/projection_image_20.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_21.png"
msgstr ".. image:: images/category_projection/projection_image_21.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_22.png"
msgstr ".. image:: images/category_projection/projection_image_22.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_23.png"
msgstr ".. image:: images/category_projection/projection_image_23.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_24.png"
msgstr ".. image:: images/category_projection/projection_image_24.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_25.png"
msgstr ".. image:: images/category_projection/projection_image_25.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_26.png"
msgstr ".. image:: images/category_projection/projection_image_26.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_27.png"
msgstr ".. image:: images/category_projection/projection_image_27.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_28.png"
msgstr ".. image:: images/category_projection/projection_image_28.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_29.png"
msgstr ".. image:: images/category_projection/projection_image_29.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_animation_02.gif"
msgstr ".. image:: images/category_projection/projection_animation_02.gif"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_30.png"
msgstr ".. image:: images/category_projection/projection_image_30.png"

#: ../../general_concepts/projection/axonometric.rst:1
msgid "Axonometric projection."
msgstr "Projecció axonomètrica."

#: ../../general_concepts/projection/axonometric.rst:10
msgid ""
"This is a continuation of :ref:`the orthographic and oblique tutorial "
"<projection_orthographic>`, be sure to check it out if you get confused!"
msgstr ""
"Aquesta és una continuació de :ref:`la guia d'aprenentatge de la projecció "
"ortogràfica i obliqua <projection_orthographic>`, assegureu-vos de comprovar-"
"la si us confoneu!"

#: ../../general_concepts/projection/axonometric.rst:12
#: ../../general_concepts/projection/axonometric.rst:16
msgid "Axonometric"
msgstr "Axonomètrica"

#: ../../general_concepts/projection/axonometric.rst:12
msgid "Projection"
msgstr "Projecció"

#: ../../general_concepts/projection/axonometric.rst:12
msgid "Dimetric"
msgstr "Dimètrica"

#: ../../general_concepts/projection/axonometric.rst:12
msgid "Isometric"
msgstr "Isomètrica"

#: ../../general_concepts/projection/axonometric.rst:18
msgid "So, the logic of adding the top is still similar to that of the side."
msgstr ""
"Per tant, la lògica d'afegir la part superior segueix sent similar a la del "
"lateral."

#: ../../general_concepts/projection/axonometric.rst:23
msgid ""
"Not very interesting. But it gets much more interesting when we use a side "
"projection:"
msgstr ""
"No és gaire interessant. Però es torna molt més interessant quan utilitzem "
"una projecció lateral:"

#: ../../general_concepts/projection/axonometric.rst:28
msgid ""
"Because our cube is red on both front-sides, and blue on both left and right "
"side, we can just use copies, this simplifies the method for cubes a lot. We "
"call this form of axonometric projection 'dimetric' as it deforms two "
"parallel lines equally."
msgstr ""
"A causa que el nostre cub és vermell en ambdós costats frontals i blau en "
"ambdós laterals, simplement podem utilitzar còpies, això simplifica molt el "
"mètode per als cubs. Anomenem «dimètrica» a aquesta forma de projecció "
"axonomètrica, ja que deforma dues línies paral·leles per igual."

#: ../../general_concepts/projection/axonometric.rst:30
msgid ""
"Isometric is sorta like dimetric where we have the same angle between all "
"main lines:"
msgstr ""
"Isomètrica és una cosa així com dimètrica on tenim el mateix angle entre "
"totes les línies principals:"

#: ../../general_concepts/projection/axonometric.rst:35
msgid ""
"True isometric is done with a 90-54.736=35.264° angle from ground plane:"
msgstr ""
"La veritable isometria es realitza amb un angle de 90-54,736=35,264° des del "
"pla del terra:"

#: ../../general_concepts/projection/axonometric.rst:40
msgid ""
"(as you can see, it doesn't line up perfectly, because Inkscape, while more "
"designed for making these kinds of diagrams than Krita, doesn't have tools "
"to manipulate the line's angle in degrees)"
msgstr ""
"(Com podeu veure, no s'alinea perfectament, perquè Inkscape, encara que està "
"més dissenyat per a fer aquest tipus de diagrames que el Krita, no té eines "
"per a manipular l'angle de la línia en graus)."

#: ../../general_concepts/projection/axonometric.rst:42
msgid ""
"This is a bit of an awkward angle, and on top of that, it doesn't line up "
"with pixels sensibly, so for videogames an angle of 30° from the ground "
"plane is used."
msgstr ""
"Aquest és un angle una mica incòmode, i a més d'això, no s'alinea amb els "
"píxels de manera sensible, de manera que per als videojocs s'utilitza un "
"angle de 30° des del pla del terra."

#: ../../general_concepts/projection/axonometric.rst:47
msgid "Alright, so, let's make an isometric out of our boy then."
msgstr "Molt bé, llavors, creem una isomètrica del nostre noi."

#: ../../general_concepts/projection/axonometric.rst:49
msgid "We make a new document, and add a vector layer."
msgstr "Creem un document nou i afegim una capa vectorial."

#: ../../general_concepts/projection/axonometric.rst:51
msgid ""
"On the vector layer, we select the straight line tool, start a line and then "
"hold the :kbd:`Shift` key to make it snap to angles. This'll allow us to "
"make a 30° setup like above:"
msgstr ""
"A la capa vectorial, seleccionem l'eina de línia recta, començarem una línia "
"i després mantindrem premuda la tecla :kbd:`Majús.` per a fer que s'ajusti "
"als angles. Això ens permetrà fer una configuració de 30° com en l'anterior:"

#: ../../general_concepts/projection/axonometric.rst:56
msgid ""
"We then import some of the frames from the animation via :menuselection:"
"`Layers --> Import/Export --> Import layer`."
msgstr ""
"Després importarem alguns dels fotogrames de l'animació a través de :"
"menuselection:`Capes --> Importa/Exporta --> Importa una capa`."

#: ../../general_concepts/projection/axonometric.rst:58
msgid ""
"Then crop it by setting the crop tool to :guilabel:`Layer`, and use :"
"menuselection:`Filters --> Colors --> Color to alpha` to remove any "
"background. I also set the layers to 50% opacity. We then align the vectors "
"to them:"
msgstr ""
"Després escapceu-la configurant l'eina d'escapçat a :guilabel:`Capa`, i "
"utilitzeu :menuselection:`Filtres --> Colors --> Color a alfa` per eliminar "
"qualsevol fons. També vaig establir l'opacitat al 50%. Després alinearem els "
"vectors a ells:"

#: ../../general_concepts/projection/axonometric.rst:65
msgid ""
"To resize a vector but keep its angle, you just select it with the shape "
"handling tool (the white arrow) drag on the corners of the bounding box to "
"start moving them, and then press the :kbd:`Shift` key to constrain the "
"ratio. This'll allow you to keep the angle."
msgstr ""
"Per a canviar la mida d'un vector però mantenint el seu angle, simplement "
"seleccioneu-lo amb l'eina de maneig de formes (la fletxa blanca), "
"arrossegant sobre les cantonades del quadre contenidor per començar a moure "
"i després premeu la tecla :kbd:`Majús.` per a restringir la relació. Això "
"permetrà mantenir l'angle."

#: ../../general_concepts/projection/axonometric.rst:67
msgid ""
"The lower image is 'the back seen from the front', we'll be using this to "
"determine where the ear should go."
msgstr ""
"La imatge inferior és «la part posterior vista des del frontal», la "
"utilitzarem per a determinar on haurà d'anar l'orella."

#: ../../general_concepts/projection/axonometric.rst:69
msgid ""
"Now, we obviously have too little space, so select the crop tool, select :"
"guilabel:`Image` and tick :guilabel:`Grow` and do the following:"
msgstr ""
"Ara, òbviament tindrem molt poc espai, de manera que trieu l'eina "
"d'escapçat, seleccioneu :guilabel:`Imatge` i marqueu :guilabel:`Fes créixer` "
"i feu el següent:"

#: ../../general_concepts/projection/axonometric.rst:74
msgid ""
"Grow is a more practical way of resizing the canvas in width and height "
"immediately."
msgstr ""
"Fes créixer és una forma més pràctica de canviar immediatament la mida del "
"llenç en amplada i alçada."

#: ../../general_concepts/projection/axonometric.rst:76
msgid ""
"Then we align the other heads and transform them by using the transform tool "
"options:"
msgstr ""
"Després alinearem els altres caps i els transformarem utilitzant les Opcions "
"de l'eina de transformació:"

#: ../../general_concepts/projection/axonometric.rst:81
msgid "(330° here is 360°-30°)"
msgstr "(330° aquí és 360°-30°)."

#: ../../general_concepts/projection/axonometric.rst:83
msgid ""
"Our rectangle we'll be working in slowly becomes visible. Now, this is a bit "
"of a difficult angle to work at, so we go to :menuselection:`Image --> "
"Rotate --> Rotate Image` and fill in 30° clockwise:"
msgstr ""
"El nostre rectangle en el qual treballarem lentament es fa visible. Ara, "
"aquest és un angle una mica difícil en el que treballar, de manera que anem "
"a :menuselection:`Imatge --> Gira --> Gira la imatge` i ompliu en 30° en el "
"sentit horari:"

#: ../../general_concepts/projection/axonometric.rst:90
msgid ""
"(of course, we could've just rotated the left two images 30°, this is mostly "
"to be less confusing compared to the cube)"
msgstr ""
"(Per descomptat, podríem haver girat les dues imatges de l'esquerra en 30°, "
"això principalment és per a ser menys confús en comparació amb el cub)."

#: ../../general_concepts/projection/axonometric.rst:92
msgid ""
"So, we do some cropping, some cleanup and add two parallel assistants like "
"we did with the orthographic:"
msgstr ""
"Llavors, fem algunes escapçades, algunes neteges i afegim dos assistents "
"paral·lels com ho vam fer amb l'ortogràfica:"

#: ../../general_concepts/projection/axonometric.rst:97
msgid ""
"So the idea here is that you draw parallel lines from both sides to find "
"points in the drawing area. You can use the previews of the assistants for "
"this to keep things clean, but I drew the lines anyway for your convenience."
msgstr ""
"Llavors, la idea és que dibuixeu línies paral·leles des d'ambdós costats "
"fins a trobar punts en l'àrea de dibuix. Utilitzeu les vistes prèvies dels "
"assistents per això i per a mantenir les coses netes, però de totes maneres "
"he dibuixat les línies per a la vostra comoditat."

#: ../../general_concepts/projection/axonometric.rst:102
msgid ""
"The best is to make a few sampling points, like with the eyebrows here, and "
"then draw the eyebrow over it."
msgstr ""
"El millor és crear alguns punts de mostreig, com amb les celles aquí, i "
"després dibuixar la cella sobre seu."

#: ../../general_concepts/projection/axonometric.rst:108
msgid "Alternative axonometric with the transform tool"
msgstr "Alternativa axonomètrica amb l'eina de transformació"

#: ../../general_concepts/projection/axonometric.rst:110
msgid ""
"Now, there's an alternative way of getting there that doesn't require as "
"much space."
msgstr ""
"Ara, hi ha una forma alternativa d'arribar que no requereix tant espai."

#: ../../general_concepts/projection/axonometric.rst:112
msgid ""
"We open our orthographic with :guilabel:`Open existing Document as Untitled "
"Document` so that we don't save over it."
msgstr ""
"Obrim la nostra ortogràfica amb :guilabel:`Obre un document existent com a "
"un document sense títol` de manera que no estalviem més."

#: ../../general_concepts/projection/axonometric.rst:114
msgid ""
"Our game-safe isometric has its angle at two pixels horizontal is one pixel "
"vertical. So, we shear the ortho graphics with transform masks to -.5/+.5 "
"pixels (this is proportional)"
msgstr ""
"La nostra isomètrica segura per als jocs té un angle de dos píxels en "
"horitzontal i un píxel en vertical. Llavors, inclinarem els gràfics "
"ortogràfics amb màscares de transformació a -0,5/+0,5 píxels (això és "
"proporcional)."

#: ../../general_concepts/projection/axonometric.rst:119
msgid ""
"Use the grid to setup two parallel rulers that represent both diagonals (you "
"can snap them with the :kbd:`Shift + S` shortcut):"
msgstr ""
"Utilitzeu la quadrícula per a configurar dos regles paral·lels que "
"representaran ambdues diagonals (ho podeu ajustar amb la drecera :kbd:"
"`Majús. + S`):"

#: ../../general_concepts/projection/axonometric.rst:124
msgid "Add the top view as well:"
msgstr "Afegiu també la vista superior:"

#: ../../general_concepts/projection/axonometric.rst:129
msgid "if you do this for all slices, you get something like this:"
msgstr "Si feu això per a totes les divisions, obtindreu quelcom com això:"

#: ../../general_concepts/projection/axonometric.rst:134
msgid ""
"Using the parallel rulers, you can then figure out the position of a point "
"in 3d-ish space:"
msgstr ""
"Utilitzant els regles paral·lels, podreu esbrinar la posició d'un punt a "
"l'espai en 3D:"

#: ../../general_concepts/projection/axonometric.rst:139
msgid "As you can see, this version both looks more 3d as well as more creepy."
msgstr ""
"Com podeu veure, aquesta versió es veu més en 3D, així com més esgarrifosa."

#: ../../general_concepts/projection/axonometric.rst:141
msgid ""
"That's because there are less steps involved as the previous version -- "
"We're deriving our image directly from the orthographic view -- so there are "
"less errors involved."
msgstr ""
"Això es deu al fet que hi ha menys passos involucrats que en la versió "
"anterior -hem fet derivar la nostra imatge directament de la vista "
"ortogràfica- de manera que hi ha menys errors involucrats."

#: ../../general_concepts/projection/axonometric.rst:143
msgid ""
"The creepiness is because we've had the tiniest bit of stylisation in our "
"side view, so the eyes come out HUGE. This is because when we stylize the "
"side view of an eye, we tend to draw it not perfectly from the side, but "
"rather slightly at an angle. If you look carefully at the turntable, the "
"same problem crops up there as well."
msgstr ""
"L'aparença esgarrifosa es deu al fet que hem tingut una mica d'estilització "
"en la nostra vista lateral, de manera que els ulls surten ENORMES. Això es "
"deu al fet que quan estilitzem la vista lateral d'un ull, tendim a no "
"dibuixar-lo perfectament des d'un costat, sinó més aviat lleugerament en "
"angle. Si observeu detingudament la taula de gir, allà també sorgeix el "
"mateix problema d'escapçat."

#: ../../general_concepts/projection/axonometric.rst:145
msgid ""
"Generally, stylized stuff tends to fall apart in 3d view, and you might need "
"to make some choices on how to make it work."
msgstr ""
"En general, les coses estilitzades tendeixen a enfonsar-se a la vista en 3D, "
"i és possible que necessiteu prendre algunes decisions sobre com fer-ho."

#: ../../general_concepts/projection/axonometric.rst:147
msgid ""
"For example, we can just easily fix the side view (because we used transform "
"masks, this is easy.)"
msgstr ""
"Per exemple, podem ajustar amb facilitat la vista lateral (això és fàcil "
"perquè utilitzem màscares de transformació)."

#: ../../general_concepts/projection/axonometric.rst:152
msgid "And then generate a new drawing from that…"
msgstr "I després generem un dibuix nou a partir d'aquest..."

#: ../../general_concepts/projection/axonometric.rst:157
msgid ""
"Compare to the old one and you should be able to see that the new result’s "
"eyes are much less creepy:"
msgstr ""
"Compareu-lo amb l'anterior i hauríeu de poder veure que els ulls del nou "
"resultat són molt menys esgarrifosos:"

#: ../../general_concepts/projection/axonometric.rst:162
msgid ""
"It still feels very squashed compared to the regular parallel projection "
"above, and it might be an idea to not just skew but also stretch the orthos "
"a bit."
msgstr ""
"Encara se sent molt aixafada en comparació amb la projecció paral·lela "
"regular anterior, i podria ser una idea no només inclinar sinó també estirar "
"una mica les projeccions ortogràfiques."

#: ../../general_concepts/projection/axonometric.rst:164
msgid "Let's continue with perspective projection in the next one!"
msgstr "Continuem amb la projecció en perspectiva en la següent!"
