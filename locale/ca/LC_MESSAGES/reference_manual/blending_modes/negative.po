# Translation of docs_krita_org_reference_manual___blending_modes___negative.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
# Josep Ma. Ferrer <txemaq@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-09 19:03+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../reference_manual/blending_modes/negative.rst:1
msgid ""
"Page about the negative blending modes in Krita: Additive Subtractive, Arcus "
"Tangent, Difference, Equivalence, Exclusion, Negation."
msgstr ""
"Pàgina sobre els modes de barreja negativa en el Krita: Additiu subtractiu, "
"Arc tangent, Diferència, Equivalència, Exclusió i Negació."

#: ../../reference_manual/blending_modes/negative.rst:12
#: ../../reference_manual/blending_modes/negative.rst:16
msgid "Negative"
msgstr "Negatiu"

#: ../../reference_manual/blending_modes/negative.rst:18
msgid "These are all blending modes which seem to make the image go negative."
msgstr ""
"Aquests són tots els modes de barreja que fan que la imatge sembli un "
"negatiu."

#: ../../reference_manual/blending_modes/negative.rst:20
#: ../../reference_manual/blending_modes/negative.rst:24
msgid "Additive Subtractive"
msgstr "Additiu subtractiu"

#: ../../reference_manual/blending_modes/negative.rst:25
msgid "Subtract the square root of the lower layer from the upper layer."
msgstr "Sostreu l'arrel quadrada de la capa inferior de la capa superior."

#: ../../reference_manual/blending_modes/negative.rst:30
msgid ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Additive_Subtractive_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Additive_Subtractive_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/negative.rst:30
msgid "Left: **Normal**. Right: **Additive Subtractive**."
msgstr "Esquerra: **Normal**. Dreta: **Additiu subtractiu**."

#: ../../reference_manual/blending_modes/negative.rst:32
#: ../../reference_manual/blending_modes/negative.rst:36
msgid "Arcus Tangent"
msgstr "Arc tangent"

#: ../../reference_manual/blending_modes/negative.rst:38
msgid ""
"Divides the lower layer by the top. Then divides this by Pi. Then uses that "
"in an Arc tangent function, and multiplies it by two."
msgstr ""
"Divideix la capa inferior per la de la part superior. Després divideix el "
"valor per Pi. Finalment, utilitza el resultat en una funció d'arc tangent i "
"el multiplica per dos."

#: ../../reference_manual/blending_modes/negative.rst:44
msgid ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Arcus_Tangent_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Arcus_Tangent_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/negative.rst:44
msgid "Left: **Normal**. Right: **Arcus Tangent**."
msgstr "Esquerra: **Normal**. Dreta: **Arc tangent**."

#: ../../reference_manual/blending_modes/negative.rst:46
#: ../../reference_manual/blending_modes/negative.rst:50
msgid "Difference"
msgstr "Diferència"

#: ../../reference_manual/blending_modes/negative.rst:52
msgid ""
"Checks per pixel of which layer the pixel-value is highest/lowest, and then "
"subtracts the lower value from the higher-value."
msgstr ""
"Verifica, píxel per píxel, quina capa té el valor del píxel més gran/més "
"petit, després resta el més petit del més gran."

#: ../../reference_manual/blending_modes/negative.rst:58
msgid ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Difference_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Difference_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/negative.rst:58
msgid "Left: **Normal**. Right: **Difference**."
msgstr "Esquerra: **Normal**. Dreta: **Diferència**."

#: ../../reference_manual/blending_modes/negative.rst:60
#: ../../reference_manual/blending_modes/negative.rst:64
msgid "Equivalence"
msgstr "Equivalència"

#: ../../reference_manual/blending_modes/negative.rst:66
msgid ""
"Subtracts the underlying layer from the upper-layer. Then inverts that. "
"Seems to produce the same result as :ref:`bm_difference`."
msgstr ""
"Sostreu la capa subjacent de la capa superior. Després ho inverteix. Sembla "
"produir el mateix resultat que la :ref:`bm_difference`."

#: ../../reference_manual/blending_modes/negative.rst:72
msgid ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Equivalence_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Equivalence_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/negative.rst:72
msgid "Left: **Normal**. Right: **Equivalence**."
msgstr "Esquerra: **Normal**. Dreta: **Equivalència**."

#: ../../reference_manual/blending_modes/negative.rst:74
#: ../../reference_manual/blending_modes/negative.rst:78
msgid "Exclusion"
msgstr "Exclusió"

#: ../../reference_manual/blending_modes/negative.rst:80
msgid ""
"This multiplies the two layers, adds the source, and then subtracts the "
"multiple of two layers twice."
msgstr ""
"Això multiplica les dues capes, afegeix l'origen i després sostreu dues "
"vegades el múltiple de les dues capes."

#: ../../reference_manual/blending_modes/negative.rst:85
msgid ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Exclusion_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Exclusion_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/negative.rst:85
msgid "Left: **Normal**. Right: **Exclusion**."
msgstr "Esquerra: **Normal**. Dreta: **Exclusió**."

#: ../../reference_manual/blending_modes/negative.rst:87
#: ../../reference_manual/blending_modes/negative.rst:91
msgid "Negation"
msgstr "Negació"

#: ../../reference_manual/blending_modes/negative.rst:93
msgid ""
"The absolute of the 1.0f value subtracted by base subtracted by the blend "
"layer. abs(1.0f - Base - Blend)"
msgstr ""
"L'absolut del valor 1,0f restat per la base restada per la capa de la "
"barreja. abs(1,0f - Base - Barreja)"

#: ../../reference_manual/blending_modes/negative.rst:98
msgid ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Negation_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Negation_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/negative.rst:98
msgid "Left: **Normal**. Right: **Negation**."
msgstr "Esquerra: **Normal**. Dreta: **Negació**."
