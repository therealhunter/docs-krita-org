# Translation of docs_krita_org_reference_manual___dockers___undo_history.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-24 16:41+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

# skip-rule: punctuation-period
#: ../../<generated>:1
msgid "Split strokes."
msgstr "Divideix els traços"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: clic dret del ratolí"

#: ../../reference_manual/dockers/undo_history.rst:1
msgid "Overview of the undo history docker."
msgstr "Vista general de l'acoblador Historial de desfer."

#: ../../reference_manual/dockers/undo_history.rst:11
msgid "Undo"
msgstr "Desfés"

#: ../../reference_manual/dockers/undo_history.rst:11
msgid "Redo"
msgstr "Refés"

#: ../../reference_manual/dockers/undo_history.rst:11
msgid "History"
msgstr "Historial"

#: ../../reference_manual/dockers/undo_history.rst:16
msgid "Undo History"
msgstr "Historial de desfer"

#: ../../reference_manual/dockers/undo_history.rst:19
msgid ".. image:: images/dockers/Krita_Undo_History_Docker.png"
msgstr ".. image:: images/dockers/Krita_Undo_History_Docker.png"

#: ../../reference_manual/dockers/undo_history.rst:20
msgid ""
"This docker allows you to quickly shift between undo states, and even go "
"back in time far more quickly that rapidly reusing the :kbd:`Ctrl + Z` "
"shortcut."
msgstr ""
"Aquest acoblador permet canviar ràpidament entre els estats de desfer, i "
"fins i tot retrocedir en el temps molt més ràpidament que reutilitzant "
"ràpidament la drecera :kbd:`Ctrl + Z`."

#: ../../reference_manual/dockers/undo_history.rst:22
msgid "Cumulate Undo"
msgstr "Desfer acumulatiu"

#: ../../reference_manual/dockers/undo_history.rst:25
msgid "Cumulative Undo"
msgstr "Desfer acumulatiu"

#: ../../reference_manual/dockers/undo_history.rst:27
msgid ""
"|mouseright| an item in the undo-history docker to enable cumulative undo. |"
"mouseright| again to change the parameters:"
msgstr ""
"Fer |mouseright| sobre un element de l'acoblador Historial de desfer per "
"habilitar el desfer acumulatiu. Feu |mouseright| de nou per a canviar els "
"paràmetres:"

#: ../../reference_manual/dockers/undo_history.rst:29
msgid "Start merging time"
msgstr "Inicia el temps de fusió"

#: ../../reference_manual/dockers/undo_history.rst:30
msgid ""
"The amount of seconds required to consider a group of strokes to be worth "
"one undo step."
msgstr ""
"La quantitat de segons necessària per a considerar que un grup de traços "
"valdrà com un pas de desfer."

#: ../../reference_manual/dockers/undo_history.rst:31
msgid "Group time"
msgstr "Temps de grup"

#: ../../reference_manual/dockers/undo_history.rst:32
msgid ""
"According to this parameter -- groups are made. Every stroke is put into the "
"same group till two consecutive strokes have a time gap of more than T "
"seconds. Then a new group is started."
msgstr ""
"Segons aquest paràmetre -com es fan els grups-. Cada traç es posarà al "
"mateix grup fins que dos traços consecutius tinguin un interval de temps "
"superior a «T» segons. Després s'iniciarà un grup nou."

#: ../../reference_manual/dockers/undo_history.rst:34
msgid ""
"A user may want to keep the ability of Undoing/Redoing his last N strokes. "
"Once N is crossed -- the earlier strokes are merged into the group's first "
"stroke."
msgstr ""
"Un usuari pot voler mantenir la capacitat de Desfés/Refés els últims «N» "
"traços. Una vegada es creua «N», els traços anteriors es fusionaran dins del "
"traç del primer grup."
