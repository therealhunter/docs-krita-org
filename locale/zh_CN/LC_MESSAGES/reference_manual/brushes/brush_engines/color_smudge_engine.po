msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-16 17:05\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___brushes___brush_engines___color_smudge_engine."
"pot\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: 鼠标左键"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:1
msgid "The Color Smudge Brush Engine manual page."
msgstr "介绍 Krita 的颜色涂抹笔刷引擎。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:13
#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:18
msgid "Color Smudge Brush Engine"
msgstr "颜色涂抹笔刷引擎"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:13
msgid "Brush Engine"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:13
msgid "Color Mixing"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:13
msgid "Smudge"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:22
msgid ".. image:: images/icons/colorsmudge.svg"
msgstr ".. image:: images/icons/colorsmudge.svg"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:23
msgid ""
"The Color Smudge Brush is a brush engine that allows you to mix colors by "
"smearing or dulling. A very powerful brush engine to the painter."
msgstr ""
"这个笔刷引擎通过涂抹和钝化来混合颜色，是一个能在上色流程中大展身手的强大引"
"擎。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:26
msgid "Options"
msgstr "可用笔刷选项"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:29
msgid ":ref:`option_brush_tip`"
msgstr ":ref:`option_brush_tip`"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:30
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:31
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:32
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:33
msgid ":ref:`option_spacing`"
msgstr ":ref:`option_spacing`"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:34
msgid ":ref:`option_mirror`"
msgstr ":ref:`option_mirror`"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:35
msgid ":ref:`option_softness`"
msgstr ":ref:`option_softness`"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:36
msgid ":ref:`option_rotation`"
msgstr ":ref:`option_rotation`"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:37
msgid ":ref:`option_scatter`"
msgstr ":ref:`option_scatter`"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:38
msgid ":ref:`option_gradient`"
msgstr ":ref:`option_gradient`"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:39
msgid ":ref:`option_airbrush`"
msgstr ":ref:`option_airbrush`"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:40
msgid ":ref:`option_texture`"
msgstr ":ref:`option_texture`"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:44
msgid "Options Unique to the Color Smudge Brush"
msgstr "颜色涂抹引擎的特有选项"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:49
msgid "Color Rate"
msgstr "颜色量"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:51
msgid ""
"How much of the foreground color is added to the smudging mix. Works "
"together with :ref:`option_smudge_length` and :ref:`option_smudge_radius`."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:54
msgid ".. image:: images/brushes/Krita_2_9_brushengine_colorrate_04.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:58
#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:115
msgid "Smudge Length"
msgstr "涂抹长度"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:60
msgid "Affects smudging and allows you to set it to Sensors."
msgstr "此项控制涂抹的方式和传感器对涂抹长度的影响。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:62
msgid "There are two major types:"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:65
msgid ".. image:: images/brushes/Krita_2.9_brush_engine_smudge_length_03.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:66
msgid "Smearing"
msgstr "涂抹"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:67
msgid "Great for making brushes that have a very impasto oil feel to them."
msgstr "适合用来制作一些具有油腻团块感的的笔刷。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:69
msgid "Dulling"
msgstr "钝化"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:69
msgid "Named so because it dulls strong colors."
msgstr "会让鲜明的颜色变得黯淡，因而得名。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:71
msgid ""
"Using an arithmetic blending type, Dulling is great for more smooth type of "
"painting."
msgstr ""
"下图上方是涂抹模式的效果，下方是钝化模式的效果。钝化模式更适用于绘制平滑过渡"
"的效果。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:74
msgid ".. image:: images/brushes/Krita_2.9_brushengine_smudge_length_01.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:76
msgid "Strength"
msgstr "强度"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:76
msgid ""
"Affects how much the smudge length takes from the previous dab its sampling. "
"This means that smudge-length at 1.0 will never decrease, but smudge-lengths "
"under that will decrease based on spacing and opacity/flow."
msgstr ""
"此选项控制前一个笔尖印迹被当前笔尖印迹采样时的颜色衰减量。如果强度为 100%，则"
"采样得到的颜色不会衰减。要注意的是：“不透明度”和“流量”笔刷选项也会造成颜色衰"
"减，所以要想得到下图一样的明显区别，记得关闭不透明度和流量的压感笔传感器映"
"射。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:79
msgid ".. image:: images/brushes/Krita_2.9_brushengine_smudge_length_02.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:83
msgid "Smudge Radius"
msgstr "涂抹半径"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:85
msgid ""
"The :guilabel:`Smudge Radius` allows you to sample a larger radius when "
"using smudge-length in :guilabel:`Dulling` mode."
msgstr "此选项控制在 :guilabel:`钝化` 模式下涂抹采样半径的大小。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:87
msgid ""
"The slider is percentage of the brush-size. You can have it modified with :"
"guilabel:`Sensors`."
msgstr ""
"此选项的“效果强度”滑动条控制的是涂抹半径相对笔刷大小的百分比。你可以通过压感"
"笔传感器来控制它的数值。下图从上到下依次为为涂抹半径为 0、100% 笔刷大小、"
"200% 笔刷大小时的效果。可见涂抹半径越大，涂抹采样的范围就越广，得到的颜色就越"
"接近采样区域的平均值。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:90
msgid ".. image:: images/brushes/Krita_2.9_brushengine_smudge_radius_01.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:92
msgid "Overlay"
msgstr "覆盖模式"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:94
msgid ""
"Overlay is a toggle that determine whether or not the smudge brush will "
"sample all layers (overlay on), or only the current one."
msgstr ""
"勾选此项后，涂抹笔刷将对所有图层进行采样。一般在图层列表的最顶层使用，因为顾"
"名思义，涂抹的效果将覆盖下面各个图层。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:97
msgid "Tutorial: Color Smudge Brushes"
msgstr "颜色涂抹笔刷引擎使用教程"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:99
msgid ""
"I recommend at least skimming over the first part to get an idea of what "
"does what."
msgstr ""
"本节的部分内容较旧且与前面重复，但我们建议至少粗略阅读本教程的第一部分，因为"
"它们从另外的角度解释了引擎的工作原理，可以帮助你更好地理解各个选项的作用。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:102
msgid "Overview and settings"
msgstr "概述和选项功能介绍"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:105
msgid "Overview: Smearing and Dulling"
msgstr "涂抹和钝化模式"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:107
msgid ""
"The Color Smudge Brush offers 2 modes, accessible from the :guilabel:`Smudge "
"Rate` section:"
msgstr "颜色涂抹笔刷引擎在 :guilabel:`涂抹长度` 选项页面提供了两种工作模式："

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:109
msgid ""
"Smearing: This mode mixes colors by smudging (\"smearing\") the area "
"underneath."
msgstr "涂抹：把光标下方的颜色通过直接涂抹的方式进行混合。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:110
msgid ""
"Dulling: In his mode, the brush \"picks up\" the color underneath it, mixes "
"it with its own color, then paints with it."
msgstr ""
"钝化：拾取光标下方的颜色，把它与笔刷颜色混合后得到新颜色，然后用这种新颜色作"
"画。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:113
msgid ".. image:: images/brushes/Krita-tutorial5-I.1.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:117
msgid ""
"To better demonstrate the smudge function, I turned the color rate function "
"off."
msgstr ""
"为了更好地演示涂抹长度选项的效果，在下面的例子中“颜色量”选项已被关闭。上半部"
"分为涂抹模式，下半部分为钝化模式。生效选项从左到右依次为：1) 涂抹长度；2) 透"
"明度；3) 淡化 (笔尖)；4) 间距 (笔尖，涂抹长度 100%)；5) 密度 (笔尖)。注意：在"
"新版中数值已改为百分比，截图中的 1.00 相当于 100%。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:120
msgid ".. image:: images/brushes/Krita-tutorial5-I.2.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:121
msgid "Common behaviors:"
msgstr "常见误区："

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:123
msgid ""
"Unchecking the smudge rate function sets smudge rate to 1.00 (not 0.00)."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:124
msgid ""
"Opacity: Below 0.50, there is practically no smudging left: keep opacity "
"over 0.50."
msgstr ""
"不透明度在 50% 以下时基本就不会产生涂抹效果了，所以不要把不透明度调得太低。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:126
msgid "Differences:"
msgstr "各选项的效果对比："

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:128
msgid ""
"Spacing with Smearing: the lower the spacing, the smoother the effect, so "
"for smearing with a round brush you may prefer a value of 0.05 or less. "
"Spacing affects the length of the smudge trail, but to a much lesser extent. "
"The \"strength\" of the effect remains more or less the same however."
msgstr ""
"涂抹模式下的间距效果：间距越小，效果越平滑。如果使用圆形笔尖进行涂抹，最好把"
"间距设为 0.05 以下。间距对涂抹痕迹的长度有微妙的影响，但涂抹长度的效果强度依"
"然是涂抹痕迹长度的首要影响因素。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:129
msgid ""
"Spacing with Dulling: the lower the spacing, the stronger the effect: "
"lowering the spacing too much can make the dulling effect too strong (it "
"picks up a color and never lets go of it). The length of the effect is also "
"affected."
msgstr ""
"钝化模式下的间距效果：间距越小，效果越明显。随意调低间距将把钝化效果变得过于"
"明显，因为它拾取一种颜色后通过此颜色绘制的时间更长。间距也会直接决定了钝化作"
"用痕迹的长度。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:130
msgid ""
"Both Smearing and Dulling have a \"smudge trail\", but in the case of "
"Dulling, the brush shape is preserved. Instead the trail determines how fast "
"the color it picked up is dropped off."
msgstr ""
"涂抹和钝化都会留下一条作用痕迹。涂抹的痕迹就像是原图像受到了推挤，而钝化的痕"
"迹能看出笔尖形状。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:133
msgid ""
"The other settings should be pretty obvious from the pictures, so I'll spare "
"you some walls of text."
msgstr "其他选项的效果可从截图中自行感受，此处不再赘述。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:136
msgid "Color Rate, Gradient and Blending modes"
msgstr "颜色量、渐变、混色模式"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:139
msgid ".. image:: images/brushes/Krita-tutorial5-I.3.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:140
msgid ""
"Again, most of the settings behaviors should be obvious from the pictures. "
"Just remember to keep :guilabel:`Opacity` over 0.50."
msgstr ""
"在上图中，上半部分为涂抹模式，下半部分为钝化模式。各选项效果从左到右依次为："
"1) 涂抹长度；2) 颜色量；3) 不透明度；4) 淡化 (笔尖)；5) 间距 (笔尖)；6) 密度 "
"(笔尖)。一图胜千言，请自行从截图中感受具体效果，不再赘述。记得将 :guilabel:`"
"不透明度` 控制在 50% 以上即可。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:143
msgid "Brush tips"
msgstr "笔尖"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:145
msgid ""
"The Color Smudge Brush has all the same brush tip options as the Pixel Brush!"
msgstr "颜色涂抹笔刷引擎可以使用像素笔刷引擎的所有笔尖选项。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:148
msgid ".. image:: images/brushes/Krita-tutorial5-I.4.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:149
msgid ""
"Just remember that the smudge effects are weaker when a brush tip's opacity "
"is lower, so for low-opacity brush tips, increase the opacity and smudge/"
"color rates."
msgstr ""
"还是那句话，笔尖不透明度太低时会导致涂抹效果不明显。如果笔尖本身的不透明度较"
"低，则应增加不透明度、涂抹长度、颜色量等选项的效果强度。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:152
msgid "Scatter and other shape dynamics"
msgstr "分散以及其他动态选项"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:154
msgid ""
"The Color Smudge Brush shares the following dynamics with the Pixel Brush: "
"Opacity, Size, Spacing, Rotation, and Scatter."
msgstr ""
"颜色涂抹笔刷引擎与像素笔刷引擎有下列共通的动态选项：不透明度、大小、间距、旋"
"转和分散。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:156
msgid ""
"However, because of the Smudge effects, the outcome will be different from "
"the Pixel Brush. In particular, the Scatter option becomes much more "
"significant."
msgstr ""
"不过由于涂抹的影响，两个引擎的同种选项会产生不同的效果。分散效果的区别尤其明"
"显。下面的例子展示了分散选项在涂抹笔刷下的效果。上半部分为涂抹模式，下半部分"
"为钝化模式。左边是分散度 50% 的效果，右边为加上了大小和旋转映射到随机度的效"
"果。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:159
msgid ".. image:: images/brushes/Krita-tutorial5-I.5-1.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:160
msgid "A few things to note:"
msgstr "注意事项："

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:162
msgid ""
"Scattering is proportional to the brush size. It's fine to use a scattering "
"of 5.00 for a tiny round brush, but for bigger brushes, you may want to get "
"it down to 0.50 or less."
msgstr ""
"分散程度与笔刷大小是成比例的。小型笔刷可以使用 500% 的分散度，但大型笔刷或许"
"就要把它调到 50% 或者更低。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:163
msgid ""
"You may notice the lines with the :guilabel:`Smearing` option. Those are "
"caused by the fact that it picked up the hard lines of the rectangle."
msgstr ""
"上图的 :guilabel:`涂抹` 模式区域产生了一些横线，这是因为笔刷拾取到了蓝色矩形"
"的边缘。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:164
msgid ""
"For scattering, the brush picks up colors within a certain distance, not the "
"color directly under the paintbrush:"
msgstr ""
"分散选项启用时，笔刷会拾取笔迹附近一定范围内的颜色，而不仅拾取笔刷下面的颜"
"色。在下图中，尽管笔迹只经过了没有颜色的区域，却拾取到了位于路径附近的红、蓝"
"色带的颜色。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:167
msgid ".. image:: images/brushes/Krita-tutorial5-I.5-2.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:169
msgid "Other color behaviors: Gradient, Blending modes, Overlay mode"
msgstr "其他颜色选项：渐变、混色模式、覆盖模式"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:172
msgid "Gradient"
msgstr "渐变"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:174
msgid ""
"Gradient is equivalent to the :menuselection:`Source --> Gradient` and :"
"menuselection:`Color --> Mix` for the Pixel brush: the color will vary "
"between the colors of the gradient."
msgstr ""
"涂抹笔刷引擎中的渐变相当于像素笔刷的 :menuselection:`来源 --> 渐变` 和 :"
"menuselection:`颜色 --> 混合` 等选项的作用：笔刷的颜色将会在渐变的颜色中变"
"化。在下面的例子中，左边是涂抹模式，右边是钝化模式；上面是把渐变映射到距离传"
"感器，下面是把渐变映射到随机度传感器。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:177
msgid ".. image:: images/brushes/Krita-tutorial5-I.6-1.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:178
msgid "You can either:"
msgstr "你可以通过下面几种方式来控制渐变颜色："

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:180
msgid ""
"Leave the default :menuselection:`Foreground --> Background gradient` "
"setting, and just change the foreground and background colors"
msgstr ""
"使用默认的”Foreground to Background (前景色到背景色)“渐变，然后改变前景色和背"
"景色。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:181
msgid "Select a more specific gradient"
msgstr "直接选取一种已有渐变。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:182
msgid "Or make custom gradients."
msgstr "制作自定义渐变然后选用它。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:185
msgid "Blending Modes"
msgstr "混色模式"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:187
msgid ""
"Blending Modes work just like with the Pixel Brush. The color used though is "
"the color from Color rate."
msgstr ""
"混色模式的效果和像素笔刷引擎下面类似，但颜色涂抹引擎在此项使用的是通过”颜色"
"量“选项控制的颜色。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:189
msgid ""
"Color Blending modes with the smudge brush are even harder to predict than "
"with the pixel brush, so I'll leave you to experiment on your own."
msgstr ""
"颜色涂抹引擎的混色模式效果比在像素引擎下面更难以预测，所以你最好多做实验，找"
"到可用的搭配。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:192
msgid "Overlay Mode"
msgstr "覆盖模式"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:194
msgid ""
"By default, the Color Smudge Brush only takes information from the layer it "
"is on. However, if you want it to take color information from all the "
"layers, you can turn on the Overlay mode."
msgstr ""
"在默认状态下，颜色涂抹笔刷引擎只会拾取当前图层的颜色信息。在勾选覆盖模式后，"
"它将拾取所有图层的颜色信息。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:196
msgid ""
"Be aware though, that it does so by \"picking up\" bits of the layer "
"underneath, which may mess up your drawing if you later make changes to the "
"layer underneath."
msgstr ""
"要注意的是，由于覆盖模式的涂抹会从下面的图层中拾取一部分图像，所以在涂抹之后"
"再对下面的图层进行加工时，颜色很可能就会对不上了。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:199
msgid "Use cases: Smudging and blending"
msgstr "使用范例：涂抹和混合"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:201
msgid "This part describes use cases with color rate off."
msgstr "本范例中的 :guilabel:`颜色量` 选项均为关闭状态。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:203
msgid ""
"I won't explain the settings for dynamics in detail, as you can find the "
"explanations in the :ref:`Pixel Brush tutorial <pixel_brush_engine>`."
msgstr ""
"关于形状和透明度动态的细节，可以参考 :ref:`像素笔刷引擎页面的可用选项介绍 "
"<pixel_brush_engine>` 。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:206
msgid "Smudging effects"
msgstr "涂抹效果"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:208
msgid "For simple smudging:"
msgstr "简单涂抹操作："

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:210
msgid "Pick the Color Smudge Brush. You can use either Smearing or Dulling."
msgstr "选择颜色涂抹笔刷引擎，在”涂抹长度“选项中按需选择”涂抹“或者”钝化“。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:212
msgid "Turn off Color Rate"
msgstr "关闭”颜色量“选项。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:214
msgid "Smudge away"
msgstr ""
"进行涂抹操作。在下面的例子中，上半部分为涂抹模式的效果，下半部分为钝化模式的"
"效果。黑色的是笔尖形状。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:217
msgid ".. image:: images/brushes/Krita-tutorial5-II.2.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:218
msgid ""
"When using lower opacity brush tips, remember to \"compensate\" for the less "
"visible effects by increasing both :guilabel:`Smudge Rate` and :guilabel:"
"`Opacity`, if necessary to maximum."
msgstr ""
"请注意：在不透明度较低时，涂抹的效果不明显。在使用不透明度较低的笔尖时，可以"
"通过增大 :guilabel:`涂抹长度` 和 :guilabel:`不透明度` 的效果强度来进行补偿。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:221
msgid "Some settings for Smearing"
msgstr "涂抹模式的运用技巧"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:223
msgid ""
"For smoother smearing, decrease spacing. Remember that spacing is "
"proportional to brush tip size. For a small round brush, 0.10 spacing is "
"fine, but for mid-sized and large brushes, decrease spacing to 0.05 or less."
msgstr ""
"要想涂抹效果更加平滑，可以降低笔尖的间距。请记得间距与笔尖大小是成比例的。小"
"型笔刷可以使用 0.10 的间距，但更大的笔刷则应使用 0.05 甚至更小的间距。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:226
msgid "Some settings for Dulling"
msgstr "钝化模式的运用技巧"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:228
msgid ""
"Lowering the spacing will also make the smudging effect stronger, so find a "
"right balance. 0.10 for most mid-sized round brushes should be fine."
msgstr ""
"减小间距会使涂抹效果变得更加明显，所以要多加试验找到所需的平衡点。中等大小的"
"圆形笔刷可以使用 0.10 左右的间距。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:229
msgid ""
"Unlike Smearing, Dulling preserves the brush shape and size, so it won't "
"\"fade off\" in size like Smearing brushes do. You can mimic that effect "
"through the simple size fade dynamic."
msgstr ""
"和涂抹模式不同，钝化模式的涂抹痕迹会体现笔刷的形状，它也不会像涂抹模式的痕迹"
"那样慢慢变小。你可以把笔刷大小映射到淡化传感器来模拟这种效果。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:232
msgid "Textured blending"
msgstr "带有肌理的混色效果"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:234
msgid ""
"In this case, what I refer to as \"Blending\" here is simply using one of "
"the following two dynamics:"
msgstr ""
"你可以通过颜色涂抹引擎营造带有颜料肌理的混色效果。你可以单独或者联合使用”旋"
"转“和”分散“选项做到这一点："

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:236
msgid ":guilabel:`Rotation` set to :guilabel:`Distance` or :guilabel:`Fuzzy`"
msgstr ""
"打开 :guilabel:`旋转` 选项，映射到 :guilabel:`距离` 或者 :guilabel:`随机度"
"` 。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:239
msgid ""
"For most mid-sized brushes you will probably want to lower the scatter rate "
"to 0.50 or lower. Higher settings are okay for tiny brushes."
msgstr ""
"对于中等大小的笔刷，应该把分散度降低到 50% 以下。小型笔刷可以使用更高的分散"
"度。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:240
msgid "And/or Scatter:"
msgstr "打开分散选项"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:240
msgid ""
"Note that Scatter picks colors within a certain distance, not the color "
"directly under the brush (see :ref:`option_brush_tip`)."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:242
msgid ""
"Optional: Pile on size and other dynamics and vary brush tips. In fact, the "
"Color Smudge brush is not a blur brush, so smudging is not a very good "
"method of \"smooth\" blending. To blend smoothly, you'll have better luck "
"with:"
msgstr ""
"要得到更复杂的肌理变化，可在前面选项的基础上叠加大小以及其他选项，也可以换用"
"不同的笔尖形状。要得到更平滑的渐变，你首先要明白颜色涂抹引擎并不擅长进行平滑"
"的混色，所以需要使用一些技巧："

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:243
msgid ""
"Building up the transition by painting with intermediate values, described "
"later"
msgstr "在渐变的路线上先绘制中间色调，我们会在后面对此进一步介绍。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:244
msgid ""
"Or using the \"blur with feathered selection\" method that I'll briefly "
"mention at the end of this tutorial."
msgstr "或者通过羽化选区使用模糊滤镜，此方法也会在页面的末尾进行介绍。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:246
msgid ""
"I've tried to achieve smooth blending with Color Smudge brush by adding "
"rotation and scatter dynamics, but honestly they looked like crap."
msgstr ""
"我们不建议通过颜色涂抹引擎配合旋转和分散选项来进行平滑的混色，从我们的使用经"
"验来看这样做的效果很不理想。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:248
msgid "However, the Color Smudge brush is very good at \"textured blending\":"
msgstr ""
"不过颜色涂抹引擎却特别擅长制作带有肌理的混色效果。在下面的例子中，黑色的是笔"
"尖形状。上排第一个是分散+预制形状笔尖，第二个是分散+大小映射到随机度+其他，第"
"三个是旋转映射到随机度 (涂抹模式)，第四个是旋转映射到随机度 (钝化模式)。下排"
"第一个是旋转映射到随机度+分散 (涂抹模式)，第二个是旋转映射到随机度+分散 (钝化"
"模式)，第三个是预制形状笔尖的涂抹模式，第四个是预制形状笔尖的钝化模式："

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:251
msgid ".. image:: images/brushes/Krita-tutorial5-II.3.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:252
msgid "Basically you can paint first and add textured transitions after."
msgstr ""
"总的来说，你可以先画出要混合的颜色，然后在它们上面添加带有肌理的过渡效果。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:255
msgid "Use cases: Coloring"
msgstr "使用范例：上色"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:257
msgid "For this last section, :guilabel:`Color Rate` is on."
msgstr "本范例中的 :guilabel:`颜色量` 选项均为开启状态。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:260
msgid "Layer options"
msgstr "图层选项"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:262
msgid ""
"Before we get started, notice that you have several possibilities for your "
"set up:"
msgstr "在开始之前，我们先介绍一下几种不同的图层安排方式："

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:264
msgid "Shading on the same layer"
msgstr ""
"第一种：在同一图层绘制底色和光影，效果参考下图上半部分的左一 (涂抹) 和左三 "
"(钝化)。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:265
msgid ""
"Shading on a separate layer, possibly making use of alpha-inheritance. The "
"brush blends with the transparency of the layer it's on. This means:"
msgstr ""
"第二种：在不同的图层上绘制底色和光影，可以通过继承不透明度功能把光影区域限制"
"在底色区域内，效果参考下图上半部分的左二 (涂抹) 和左四 (钝化)。笔刷的混色效果"
"和当前图层的透明度有关，这意味着："

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:267
msgid ""
"If the area underneath is more of less uniform, the output is actually "
"similar as if shading on the same layer"
msgstr "如果下面图层的颜色比较均匀，那么混色的效果和在同一图层中混色相近。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:269
msgid ""
"But if the area underneath is not uniform, then you'll get fewer color "
"variations."
msgstr "如果下方图层的颜色不均匀，那么颜色会发生一些区别。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:271
msgid ""
"Shading on a separate layer, using Overlay mode. Use this only if you're "
"fairly sure you don't need to adjust the layer below, or the colors may "
"become a mess."
msgstr ""
"第三种：底色和光影色分开在不同的图层，但在它们上面新建一个图层然后在覆盖模式"
"下面进行混色。使用这种方式混色之前要确保下方的图层不会再发生更改，否则颜色会"
"对不上。效果参考下图下半部分，左边为涂抹模式，右边为钝化模式。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:274
msgid ".. image:: images/brushes/Krita-tutorial5-III.1-1.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:276
msgid "Issue with transparency"
msgstr "透明度问题"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:278
msgid ""
"The Color Smudge Brush blends with transparency. What this means is that "
"when you start a new, transparent layer and \"paint\" on this layer, you "
"will nearly always get less than full opacity."
msgstr ""
"颜色涂抹引擎在混色时会计算透明度，这意味着在一个新的透明图层上进行涂抹时，得"
"到的颜色总会带有一定的透明度。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:280
msgid "Basically:"
msgstr "因此："

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:282
msgid "It may look great when you're coloring on a blank canvas"
msgstr "在颜色均匀的图层上面新建一个图层涂抹上色，一切都很正常。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:283
msgid "But it won't look so great when you add something underneath"
msgstr "但如果你在涂抹图层下面又画了点什么，下面的颜色就会干扰到涂抹的颜色。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:286
msgid ".. image:: images/brushes/Krita-tutorial5-III.1-2.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:287
msgid "The solution is pretty simple though:"
msgstr "解决办法其实很简单："

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:290
msgid ""
"With tinting, you already have the color underneath colored, so that's done"
msgstr "如果你只是要铺底色，那就在下面一层把底色铺完"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:291
msgid "Make sure you have the area underneath colored in first:"
msgstr "先把下面一层的颜色画好，然后再画上面一层"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:291
msgid "For painting, roughly color in the background layer first"
msgstr "如果是要画光影，先在背景层画出粗略的色块和调子，防止串色。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:292
msgid "Or color in the shape on a new layer and make use of alpha-inheritance"
msgstr "也可以利用继承不透明度来限定上面图层的可见区域。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:293
msgid ""
"For the last solution, use colors that contrast highly with what you're "
"using for best effect. For example, shade in the darkest shadow area first, "
"or the lightest highlights, and use the color smudge brush for the "
"contrasting color."
msgstr ""
"对于最后一种方法来说，涂抹上色时最好使用和下面一层反差最大的颜色。例如，先在"
"底层绘制最深的色调，然后在上面的图层用高亮色调涂抹上色 (下图左)。也可以反过来"
"先在底层绘制最浅的色调，然后在上面的图层对阴影区域涂抹上色 (下图右)。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:296
msgid ".. image:: images/brushes/Krita-tutorial5-III.1-3.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:298
msgid "Soft-shading"
msgstr "平滑混色"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:300
msgid "Suppose you want more or less smooth color transitions. You can either:"
msgstr "要画出比较柔和的颜色过渡，可以通过下列几种方式进行："

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:302
msgid ""
":guilabel:`Color Rate` as low as 0.10 for round brushes, higher with non "
"fully opaque brush tips."
msgstr ""
"对于圆形笔刷，应把 :guilabel:`颜色量` 的效果强度调到 10% 左右，如果笔尖带有透"
"明度，可以适当调高。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:303
msgid "Or set the :guilabel:`Smudge Rate` as low as 0.10 instead."
msgstr "也可以把 :guilabel:`涂抹长度` 的效果强度调到 10% 左右。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:304
msgid ""
"Or a combination of the two. Please try yourself for the output you like "
"best."
msgstr "也可以把前两种方式配合使用，多做试验以找到合适的数值。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:305
msgid "Optional: turn on :guilabel:`Rotation` for smoother blending"
msgstr "可选：启用 :guilabel:`旋转` 选项来增加平滑度"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:306
msgid "Optional: turn on :guilabel:`Scatter` for certain effects"
msgstr "可选：启用 :guilabel:`分散` 选项来营造某些效果"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:307
msgid ""
"Optional: fiddle with :guilabel:`Size` and :guilabel:`Opacity` dynamics as "
"necessary."
msgstr ""
"可选：启用 :guilabel:`大小` 和 :guilabel:`不透明度` 选项，看看它们能不能有所"
"帮助。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:310
msgid ".. image:: images/brushes/Krita-tutorial5-III.2-1.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:311
msgid ""
"This remains, in fact, a so-so way of making smooth transitions. It's best "
"to build up intermediate values instead. Here:"
msgstr ""
"在上面的例子中，上半部分是涂抹模式，下半部分是钝化模式。左边为不透明度 70%、"
"涂抹长度 10%、颜色量 50% 的效果。右边为不透明度 70%、涂抹长度 50%、颜色量 "
"10% 的效果。不难发现，单凭这些办法还不足以得到令人满意的平滑效果。我们还可以"
"在这个基础上再引入一些技术："

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:313
msgid ""
"I first passed over the blue area three times with a red color. I select 3 "
"shades."
msgstr ""
"把颜色的过渡色调先分成几段画出来。下图的例子中用了三段。如下图的上半部分所"
"示。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:314
msgid ""
"I color picked each of these values with the :kbd:`Ctrl +` |mouseleft| "
"shortcut, then used them in succession."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:317
msgid ".. image:: images/brushes/Krita-tutorial5-III.2-2.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:319
msgid "Painting: thick oil style"
msgstr "绘画风格：油画肌理"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:321
msgid ""
"Many of the included color smudge brush presets produce a thick oil paint-"
"like effect. This is mainly achieved with the Smearing mode on. Basically:"
msgstr ""
"Krita 自带的大多数颜色涂抹笔刷预设都会画出厚重的油画肌理。这是通过使用涂抹模"
"式实现的。它们的原理大致是："

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:325
msgid ""
"Both at 0.50 are fine for normal round brushes or fully opaque predefined "
"brushes"
msgstr ""
"对于一般圆形笔尖或者完全不透明的预制形状笔尖，两者的效果强度可设为 50% 左右。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:326
msgid "Smearing mode with high smudge and color rates"
msgstr "在涂抹模式下涂抹长度和颜色量选项均使用较高的效果强度"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:326
msgid ""
"Up to 1.00 each for brushes with less density or non fully-opaque predefined "
"brushes"
msgstr ""
"对于密度较低的笔尖或者带有透明度的预制形状笔尖，两者的效果强度可设为 100% 。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:328
msgid ""
"Add Size/Rotation/Scatter dynamics as needed. When you do this, increase "
"smudge and color rates to compensate for increased color mixing."
msgstr ""
"按需添加大小、旋转、分散等效果。启用更多效果后，可适当加大涂抹长度和颜色量的"
"效果强度来对颜色混合程度的提升进行补偿。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:331
msgid ".. image:: images/brushes/Krita-tutorial5-III.3-1.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:332
msgid ""
"One thing I really like to do is to set different foreground and background "
"colors, then turn on :menuselection:`Gradient --> Fuzzy`. Alternatively, "
"just paint with different colors in succession (bottom-right example)."
msgstr ""
"在上图的例子中，第一排：1) 默认效果；2) 密度 50%，涂抹长度 100%；3) 加上颜色"
"量 100%；4) 加上大小映射到随机度，分散 60%。第二排：1) 默认效果；2) 旋转映射"
"到随机度，涂抹长度 100%，颜色量 100%；3) 大小动态效果；4) 旋转映射到随机度，"
"涂抹长度 100%，颜色量 100%。还有一种值得推荐的技巧是：先把前景色和背景色设成"
"不同的颜色，然后选中”Foreground to Background (前景色到背景色)“ 渐变，在笔刷"
"选项列表中把”渐变“选项映射到随机度。还有一种技巧是分别用两种颜色绘制 (下图右"
"下角的例子)。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:335
msgid ".. image:: images/brushes/Krita-tutorial5-III.3-2.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:336
msgid ""
"Here's some final random stuff. With pixel brushes, you can get all sorts of "
"frill designs by using elongated brushes and setting the dynamics to "
"rotation. You won't get that with Color Smudge Brushes. Instead you'll get "
"something that looks more like... yarn. Which is cool too. Here, I just used "
"oval brushes and :menuselection:`Rotation --> Distance`."
msgstr ""
"最后再介绍一些特别的技巧。在使用像素笔刷引擎时，我们可以在拉长的笔尖上应用旋"
"转效果来制作一些纹路。而在使用颜色涂抹笔刷引擎时，这种用法会得到不同的效果，"
"看起来就像是针织一样，也很有意思。在下图的例子中，”旋转“选项被映射到了”距"
"离“传感器。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:339
msgid ".. image:: images/brushes/Krita-tutorial5-III.3-3.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:341
msgid "Painting: Digital watercolor style"
msgstr "绘画风格：数字水彩"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:343
msgid ""
"When I say \"digital watercolor\", it refers to a style often seen online, i."
"e. a soft, smooth shading style rather than realistic watercolor. For this "
"you mostly need the Dulling mode. A few things:"
msgstr ""
"本例中介绍的所谓”数字水彩“是一种柔和、平滑的上色风格，和真正的水彩有所区别。"
"这种风格主要通过钝化模式来实现，主要技巧如下："

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:345
msgid ""
"Contrary to the Smearing mode, you may want to lower opacity for normal "
"round brushes to get a smoother effect, to 0.70 for example."
msgstr ""
"和涂抹模式不同，在使用一般圆形笔刷时，要把不透明度调低才能得到比较平滑的效"
"果，如 70%。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:346
msgid "Vary the brush tip fade value as well."
msgstr "调整笔尖的淡化数值。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:347
msgid ""
"When using :guilabel:`Scatter` or other dynamics, you can choose to set "
"smudge and color values to high or low values, for different outcomes."
msgstr ""
"在使用 :guilabel:`分散` 或者其他效果时，可以把涂抹长度和颜色量的效果强度按需"
"调整，以得到不同的效果。下图第一排：1) 涂抹长度和颜色量同为 50%，不透明度 "
"70%，渐变映射到随机度；2) 加上旋转映射到随机度；3) 加上大小映射到随机度，分"
"散 60%。第二排：不同的预制形状笔刷，涂抹长度、颜色量、不透明度全部为 100%，大"
"小和旋转映射到随机度。第三排：用椭圆笔刷制作的一些效果。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:350
msgid ".. image:: images/brushes/Krita-tutorial5-III.4.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:352
msgid "Blurring"
msgstr "模糊混色技法"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:354
msgid "You can:"
msgstr "我们已经在前面提过，如果只靠颜色涂抹笔刷："

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:356
msgid "Paint then smudge, for mostly texture transitions"
msgstr "先画底色再涂抹，得到肌理比较明显的过渡。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:357
msgid "Or build up transitions by using intermediate color values"
msgstr "预先画出几段过渡色调然后逐个混合。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:359
msgid ""
"If you want even smoother effects, well, just use blur. Gaussian blur to be "
"exact."
msgstr ""
"但如果你想要更加平滑的效果，那么就直接通过高斯模糊滤镜来实现吧！在下面的例子"
"中，上半部分为高斯模糊滤镜，半径 20px。下半部分为先建立选区，然后在右键菜单中"
"点击”变形“，选中”羽化选区“，半径 20px，然后再应用高斯模糊滤镜，半径 60px。"

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:362
msgid ".. image:: images/brushes/Krita-tutorial5-III.5.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/color_smudge_engine.rst:363
msgid "And there you go. That last little trick concludes this tutorial."
msgstr ""
"到这里，这篇教程也就结束了。颜色涂抹笔刷引擎是一个功能强大，用法多变的引擎。"
"希望你可以大胆地进行探索，用它画出效果丰富的作品！"
