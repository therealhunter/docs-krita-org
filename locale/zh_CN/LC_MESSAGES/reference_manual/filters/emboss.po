msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___filters___emboss.pot\n"

#: ../../reference_manual/filters/emboss.rst:1
msgid "Overview of the emboss filters."
msgstr ""

#: ../../reference_manual/filters/emboss.rst:10
#: ../../reference_manual/filters/emboss.rst:15
msgid "Emboss"
msgstr "浮雕"

#: ../../reference_manual/filters/emboss.rst:10
msgid "Filters"
msgstr ""

#: ../../reference_manual/filters/emboss.rst:17
msgid ""
"Filters that are named by the traditional embossing technique. This filter "
"generates highlight and shadows to create an effect which makes the image "
"look like embossed. Emboss filters are usually used in the creation of "
"interesting GUI elements, and mostly used in combination with filter-layers "
"and masks."
msgstr ""

#: ../../reference_manual/filters/emboss.rst:20
msgid "Emboss Horizontal Only"
msgstr "仅水平浮雕"

#: ../../reference_manual/filters/emboss.rst:22
msgid "Only embosses horizontal lines."
msgstr ""

#: ../../reference_manual/filters/emboss.rst:25
msgid "Emboss in all Directions"
msgstr "全方向浮雕"

#: ../../reference_manual/filters/emboss.rst:27
msgid "Embosses in all possible directions."
msgstr ""

#: ../../reference_manual/filters/emboss.rst:30
msgid "Emboss (Laplacian)"
msgstr "浮雕 (拉普拉斯)"

#: ../../reference_manual/filters/emboss.rst:32
msgid "Uses the laplacian algorithm to perform embossing."
msgstr ""

#: ../../reference_manual/filters/emboss.rst:35
msgid "Emboss Vertical Only"
msgstr "仅垂直浮雕"

#: ../../reference_manual/filters/emboss.rst:37
msgid "Only embosses vertical lines."
msgstr ""

#: ../../reference_manual/filters/emboss.rst:40
msgid "Emboss with Variable depth"
msgstr "可变深度浮雕"

#: ../../reference_manual/filters/emboss.rst:42
msgid ""
"Embosses with a depth that can be set through the dialog box shown below."
msgstr ""

#: ../../reference_manual/filters/emboss.rst:45
msgid ".. image:: images/filters/Emboss-variable-depth.png"
msgstr ""

#: ../../reference_manual/filters/emboss.rst:47
msgid "Emboss Horizontal and Vertical"
msgstr "水平和垂直浮雕"

#: ../../reference_manual/filters/emboss.rst:49
msgid "Only embosses horizontal and vertical lines."
msgstr ""
