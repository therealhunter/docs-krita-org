msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-08-16 17:05\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___preferences___author_settings.pot\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../reference_manual/preferences/author_settings.rst:1
msgid "Author profile settings in Krita."
msgstr ""

#: ../../reference_manual/preferences/author_settings.rst:11
msgid "Preferences"
msgstr ""

#: ../../reference_manual/preferences/author_settings.rst:11
msgid "Settings"
msgstr ""

#: ../../reference_manual/preferences/author_settings.rst:11
msgid "Author Profile"
msgstr ""

#: ../../reference_manual/preferences/author_settings.rst:11
msgid "Metadata"
msgstr ""

#: ../../reference_manual/preferences/author_settings.rst:16
msgid "Author Profile Settings"
msgstr "作者档案设置"

#: ../../reference_manual/preferences/author_settings.rst:18
msgid ""
"Krita allows creating an author profile that you can use to store contact "
"info into your images."
msgstr ""

#: ../../reference_manual/preferences/author_settings.rst:20
msgid ""
"The main element is the author page. This page was overhauled massively in "
"4.0."
msgstr ""

#: ../../reference_manual/preferences/author_settings.rst:22
msgid ""
"By default, it will use the \"Anonymous\" profile, which contains nothing. "
"To create a new profile, press the \"+\" button, and write up a name for the "
"author profile."
msgstr ""

#: ../../reference_manual/preferences/author_settings.rst:24
msgid "You can then fill out the fields."
msgstr ""

#: ../../reference_manual/preferences/author_settings.rst:30
msgid ".. image:: images/preferences/Krita_4_0_preferences_author_page.png"
msgstr ""

#: ../../reference_manual/preferences/author_settings.rst:30
msgid ""
"The position field is special in that it has a list of hard coded common "
"artists positions it can suggest."
msgstr ""

#: ../../reference_manual/preferences/author_settings.rst:32
msgid ""
"In older versions of Krita there could only be one of each contact info. In "
"4.0, you can make as many contact entries as you'd like."
msgstr ""

#: ../../reference_manual/preferences/author_settings.rst:34
msgid ""
"Press :guilabel:`Add Contact Info`  to add an entry in the box. By default "
"it will set the type to homepage, because that is the one that causes the "
"least spam. Double |mouseleft| homepage to change the contact type. Double |"
"mouseleft| the \"New Contact Info\" text to turn it into a line edit to "
"change the value."
msgstr ""

#: ../../reference_manual/preferences/author_settings.rst:37
msgid "Using the new profile"
msgstr "使用新的作者档案文件"

#: ../../reference_manual/preferences/author_settings.rst:39
msgid ""
"To use a profile for your current drawing, go to :menuselection:`Settings --"
"> Active Author Profile` and select the name you gave your profile. Then, "
"when pressing :guilabel:`Save` on your current document, you will be able to "
"see your last author profile as the last person who saved it in :"
"menuselection:`File --> Document Information --> Author`."
msgstr ""

#: ../../reference_manual/preferences/author_settings.rst:42
msgid "Exporting author metadata to Jpeg and Png"
msgstr "导出作者元数据到 JPEG 和 PNG"

#: ../../reference_manual/preferences/author_settings.rst:46
msgid ""
"The jpeg and png export both have :guilabel:`Sign with author data` options. "
"Toggling these will store the Nickname and the *first entry in the contact "
"info* into the metadata of png or jpeg."
msgstr ""

#: ../../reference_manual/preferences/author_settings.rst:48
msgid ""
"For the above example in the screenshot, that would result in: ``ExampleMan "
"(http://example.com)`` being stored in the metadata."
msgstr ""
