# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-08 15:19+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<generated>:1
msgid "Paint Connection Line"
msgstr "Rita sammanbindningslinje"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:1
msgid "The Sketch Brush Engine manual page."
msgstr "Manualsidan för sketchpenselgränssnittet."

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:12
#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:17
msgid "Sketch Brush Engine"
msgstr "Sketchpenselgränssnitt"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:12
msgid "Brush Engine"
msgstr "Penselgränssnitt"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:12
msgid "Harmony Brush Engine"
msgstr "Harmonipenselgränssnitt"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:21
msgid ".. image:: images/icons/sketchbrush.svg"
msgstr ".. image:: images/icons/sketchbrush.svg"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:22
msgid ""
"A line based brush engine, based on the Harmony brushes. Very messy and fun."
msgstr ""
"Ett linjebaserat penselgränssnitt, baserat på harmonipenslarna. Mycket "
"rörigt och roligt."

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:25
msgid "Parameters"
msgstr "Parametrar"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:28
msgid "Has the following parameters:"
msgstr "Har följande parametrar:"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:30
msgid ":ref:`option_brush_tip`"
msgstr ":ref:`option_brush_tip`"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:31
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:32
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:33
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:34
msgid ":ref:`option_ratio`"
msgstr ":ref:`option_ratio`"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:35
msgid ":ref:`option_line_width`"
msgstr ":ref:`option_line_width`"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:36
msgid ":ref:`option_offset_scale`"
msgstr ":ref:`option_offset_scale`"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:37
msgid ":ref:`option_sketch_density`"
msgstr ":ref:`option_sketch_density`"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:38
msgid ":ref:`option_rotation`"
msgstr ":ref:`option_rotation`"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:39
msgid ":ref:`option_airbrush`"
msgstr ":ref:`option_airbrush`"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:44
msgid "Line Width"
msgstr "Linjebredd"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:46
msgid "The width of the rendered lines."
msgstr "Bredden på de återgivna linjerna."

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:49
msgid ".. image:: images/brushes/Krita_2_9_brushengine_sketch_linewidth.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_sketch_linewidth.png"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:53
msgid "Offset Scale"
msgstr "Positionskala"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:55
msgid ""
"When curve lines are formed, this value roughly determines the distance from "
"the curve lines to the connection lines:"
msgstr ""
"När kurvlinjer formas, bestämmer det här värdet i grova drag avståndet från "
"kurvllinjerna till anslutningslinjerna:"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:57
msgid ""
"This is a bit misleading, because a value of 0% and a value of 100% give "
"similar outputs, as do a value of say 30% and 70%. You could think that the "
"actual value range is between 50% and 200%."
msgstr ""
"Det är något missvisande, eftersom värdet 0 % och värdet 100 % ger liknande "
"resultat, liksom värdet 30 % och 70 %. Man kan föreställa sig att det "
"riktiga värdeintervallet är mellan 50 % och 200 %."

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:58
msgid ""
"0% and 100% correspond to the curve lines touching the connection lines "
"exactly."
msgstr ""
"0 % och 100 % motsvarar att kurvlinjerna direkt rör vid anslutningslinjerna."

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:59
msgid ""
"Above 100%, the curve lines will go further than the connection lines, "
"forming a fuzzy effect."
msgstr ""
"Ovanför 100 % går kurvlinjerna längre än anslutningslinjerna, vilket ger en "
"suddig effekt."

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:62
msgid ".. image:: images/brushes/Krita_2.9_brushengine_sketch_offset.png"
msgstr ".. image:: images/brushes/Krita_2.9_brushengine_sketch_offset.png"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:64
msgid ".. image:: images/brushes/Krita-sketch_offset_scale2.png"
msgstr ".. image:: images/brushes/Krita-sketch_offset_scale2.png"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:68
msgid "Density"
msgstr "Täthet"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:70
msgid ""
"The density of the lines. This one is highly affected by the Brush-tip, as "
"determined by the Distance Density toggle."
msgstr ""
"Linjernas täthet. Den påverkas is stor utsträckning av penselspetsen, som "
"bestäms av knappen Avståndstäthet."

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:73
msgid ".. image:: images/brushes/Krita_2.9_brushengine_sketch_density.png"
msgstr ".. image:: images/brushes/Krita_2.9_brushengine_sketch_density.png"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:74
msgid "Use Distance Density"
msgstr "Använd avståndstäthet"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:75
msgid ""
"The further the line covered is from the center of the area of effect, the "
"less the density of the resulting curve lines."
msgstr ""
"Ju längre från centrum av effektområdet som den täckta linjen finns, desto "
"mindre är de resulterande kurvlinjernas täthet."

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:77
msgid "Magnetify"
msgstr "Magnetifiera"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:77
msgid ""
"Magnetify is *on* by default. It's what causes curve lines to form between "
"two close line sections, as though the curve lines are attracted to them "
"like magnets. With Magnetify *off*, the curve line just forms on either side "
"of the current active portion of your connection line. In other words, your "
"line becomes fuzzier when another portion of the line is nearby, but the "
"lines don't connect to said previous portion."
msgstr ""
"Magnetifiera är normalt *på*. Det är vad som gör att kurvlinjer skapas "
"mellan två intilliggande linjesektioner, såsom att kurvlinjerna attraheras "
"till dem som magneter. När magnetifiera är *av*, skapas kurvlinjerna bara på "
"varje sida om den del av anslutningslinjen som för närvarande är aktiv. Med "
"andra ord blir linjen suddigare när en annan del av linjen finns i närheten, "
"men linjerna ansluts inte till tidigare nämnda del."

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:79
msgid "Random RGB"
msgstr "Slumpmässig RGB"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:80
msgid "Causes some slight RGB variations."
msgstr "Orsakar några mindre RGB-variationer."

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:81
msgid "Random Opacity"
msgstr "Slumpmässig ogenomskinlighet"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:82
msgid ""
"The curve lines get random opacity. This one is barely visible, so for the "
"example I used line width 12 and 100% opacity."
msgstr ""
"Kurvlinjerna får slumpmässig ogenomskinlighet. Det är nätt och jämnt "
"synligt, så jag använda exempelvis linjebredd 12 och 100 % ogenomskinlighet."

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:83
msgid "Distance Opacity"
msgstr "Avståndsogenomskinlighet"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:84
msgid ""
"The distance based opacity. When you move your pen fast when painting, the "
"opacity will be calculated based on the distance from the center of the "
"effect area."
msgstr ""
"Den avståndsbaserade ogenomskinligheten. När pennan rörs snabbare under "
"målning, beräknas ogenomskinligheten på avståndet från centrum av "
"effektområdet."

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:85
msgid "Simple Mode"
msgstr "Enkelt läge"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:86
msgid ""
"This mode exists for performance reasons, and doesn't affect the output in a "
"visible way. Check this for large brushes or thick lines for faster "
"rendering."
msgstr ""
"Läget finns av prestandaskäl, och påverkar inte resultatet på något synligt "
"sätt. Markera det för stora penslar eller tjocka linjer för snabbare "
"återgivning."

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:88
msgid ""
"What appears to be the connection line is usually made up of an actual "
"connection line and many smaller curve lines. The many small curve lines "
"make up the majority of the line. For this reason, the only time this option "
"will make a visible difference is if you're drawing with 0% or near 0% "
"density, and with a thick line width. The rest of the time, this option "
"won't make a visible difference."
msgstr ""
"Vad som verkar vara anslutningslinjen består oftast av en verklig "
"anslutningslinje och många mindre kurvlinjer. De många små kurvlinjerna "
"utgör majoriteten av linjen. Av detta skäl är den enda gången då "
"alternativet orsakar en synlig skillnad när man ritar med 0 % eller nära 0 % "
"täthet, och med en stor linjebredd. Andra gånger gör inte alternativet någon "
"synlig skillnad."
