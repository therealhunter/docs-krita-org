# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 12:26+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: vänsterknapp"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: högerknapp"

#: ../../<rst_epilog>:66
msgid ""
".. image:: images/icons/rectangular_select_tool.svg\n"
"   :alt: toolselectrect"
msgstr ""
".. image:: images/icons/rectangular_select_tool.svg\n"
"   :alt: Rektangelmarkeringsverktyg"

#: ../../reference_manual/tools/rectangular_select.rst:1
msgid "Krita's rectangular selection tool reference."
msgstr "Referens för Kritas rektangelmarkeringsverktyg."

#: ../../reference_manual/tools/rectangular_select.rst:11
msgid "Tools"
msgstr "Verktyg"

#: ../../reference_manual/tools/rectangular_select.rst:11
msgid "Selection"
msgstr "Markering"

#: ../../reference_manual/tools/rectangular_select.rst:11
msgid "Rectangle"
msgstr "Rektangel"

#: ../../reference_manual/tools/rectangular_select.rst:11
msgid "Rectangular Selection"
msgstr "Rektangelmarkering"

#: ../../reference_manual/tools/rectangular_select.rst:16
msgid "Rectangular Selection Tool"
msgstr "Rektangelmarkeringsverktyg"

#: ../../reference_manual/tools/rectangular_select.rst:18
msgid "|toolselectrect|"
msgstr "|toolselectrect|"

#: ../../reference_manual/tools/rectangular_select.rst:20
msgid ""
"This tool, represented by a rectangle with a dashed border, allows you to "
"make :ref:`selections_basics` of a rectangular area. Simply click and drag "
"around the section you wish to select."
msgstr ""
"Verktyget, som representeras av en rektangel med streckad kant, låter dig "
"skapa :ref:`selections_basics` av ett rektangulärt område. Klicka och dra "
"helt enkelt omkring området som ska markeras."

#: ../../reference_manual/tools/rectangular_select.rst:23
msgid "Hotkeys and Stickykeys"
msgstr "Snabbtangenter och klistriga tangenter"

#: ../../reference_manual/tools/rectangular_select.rst:25
msgid ":kbd:`J` selects this tool."
msgstr ":kbd:`J` väljer verktyget."

#: ../../reference_manual/tools/rectangular_select.rst:26
msgid ""
":kbd:`R` sets the selection to 'replace' in the tool options, this is the "
"default mode."
msgstr ""
":kbd:`R` ställer in markeringen till 'ersätt' i verktygsalternativen, vilket "
"är standardinställningen."

#: ../../reference_manual/tools/rectangular_select.rst:27
msgid ":kbd:`A` sets the selection to 'add' in the tool options."
msgstr ":kbd:`A` ställer in markeringen till 'addera' i verktygsalternativen."

#: ../../reference_manual/tools/rectangular_select.rst:28
msgid ":kbd:`S` sets the selection to 'subtract' in the tool options."
msgstr ""
":kbd:`S` ställer in markeringen till 'subtrahera' i verktygsalternativen."

#: ../../reference_manual/tools/rectangular_select.rst:29
msgid ""
":kbd:`Shift` after starting the selection, constraints it to a perfect "
"square."
msgstr ""
":kbd:`Skift` efter att markeringen har påbörjats, begränsar den till en "
"perfekt cirkel."

#: ../../reference_manual/tools/rectangular_select.rst:30
msgid ""
":kbd:`Ctrl` after starting the selection, makes the selection resize from "
"center."
msgstr ""
":kbd:`Ctrl` efter att markeringen har påbörjats, gör att markeringens "
"storlek ändras från centrum."

#: ../../reference_manual/tools/rectangular_select.rst:31
msgid ":kbd:`Alt` after starting the selection, allows you to move it."
msgstr ""
":kbd:`Alt` efter att markeringen har påbörjats, gör att den kan flyttas."

#: ../../reference_manual/tools/rectangular_select.rst:32
msgid ""
":kbd:`Shift +` |mouseleft| sets the subsequent selection to 'add'. You can "
"release the :kbd:`Shift` key while dragging, but it will still be set to "
"'add'. Same for the others."
msgstr ""
":kbd:`Skift +` vänster musknapp in markeringen till 'addera'. Man kan släppa "
"upp tangenten :kbd:`Skift` medan man drar, och den ställs ändå in till 'lägg "
"till'. Samma sak för de andra."

#: ../../reference_manual/tools/rectangular_select.rst:33
msgid ":kbd:`Alt +` |mouseleft| sets the subsequent selection to 'subtract'."
msgstr ""
":kbd:`Alt +` vänster musknapp ställer in efterföljande markering till "
"'subtrahera'."

#: ../../reference_manual/tools/rectangular_select.rst:34
msgid ":kbd:`Ctrl +` |mouseleft| sets the subsequent selection to 'replace'."
msgstr ""
":kbd:`Ctrl +` vänster musknapp ställer in efterföljande markering till "
"'ersätt'."

#: ../../reference_manual/tools/rectangular_select.rst:35
msgid ""
":kbd:`Shift + Alt +` |mouseleft| sets the subsequent selection to "
"'intersect'."
msgstr ""
":kbd:`Shift + Alt +` vänster musknapp ställer in efterföljande markering "
"till 'snitt'."

#: ../../reference_manual/tools/rectangular_select.rst:39
msgid "Hovering over a selection allows you to move it."
msgstr "Att hålla musen över en markering göt det möjlig att flytta den."

#: ../../reference_manual/tools/rectangular_select.rst:40
msgid ""
"|mouseright| will open up a selection quick menu with amongst others the "
"ability to edit the selection."
msgstr ""
"Att klicka med höger musknapp visar en snabbvalsmeny med bland annat "
"möjlighet att redigera markeringen."

#: ../../reference_manual/tools/rectangular_select.rst:44
msgid ""
"So to subtract a perfect square, you do :kbd:`Alt +` |mouseleft|, then "
"release the :kbd:`Alt` key while dragging and press the :kbd:`Shift` key to "
"constrain."
msgstr ""
"Så för att subtrahera en perfekt kvadrat, använder man :kbd:`Alt + vänster "
"musknapp, och släpper därefter tangenten :kbd:`Alt` medan man drar och "
"håller nere tangenten :kbd:`Shift` för begränsning."

#: ../../reference_manual/tools/rectangular_select.rst:49
msgid ""
"You can switch the behavior of the :kbd:`Alt` key to use the :kbd:`Ctrl` key "
"instead by toggling the switch in the :ref:`general_settings`."
msgstr ""
"Man kan ändra beteende hos tangenten :kbd:`Alt` så att tangenten :kbd:`Ctrl` "
"används istället genom att ställa om inställningen under :ref:"
"`general_settings`."

#: ../../reference_manual/tools/rectangular_select.rst:52
msgid "Tool Options"
msgstr "Verktygsalternativ"

#: ../../reference_manual/tools/rectangular_select.rst:54
msgid "Anti-aliasing"
msgstr "Kantutjämning"

#: ../../reference_manual/tools/rectangular_select.rst:55
msgid ""
"This toggles whether or not to give selections feathered edges. Some people "
"prefer hard-jagged edges for their selections."
msgstr ""
"Ändrar om markeringar ska få vävkanter. Vissa människor föredrar skarpa "
"naggade kanter för sina markeringar."

#: ../../reference_manual/tools/rectangular_select.rst:56
msgid "Width"
msgstr "Bredd"

#: ../../reference_manual/tools/rectangular_select.rst:57
msgid ""
"Gives the current width. Use the lock to force the next selection made to "
"this width."
msgstr ""
"Ger den aktuella bredden. Använd lås för att tvinga nästa markering att få "
"den här bredden."

#: ../../reference_manual/tools/rectangular_select.rst:58
msgid "Height"
msgstr "Höjd"

#: ../../reference_manual/tools/rectangular_select.rst:59
msgid ""
"Gives the current height. Use the lock to force the next selection made to "
"this height."
msgstr ""
"Ger den aktuella höjden. Använd lås för att tvinga nästa markering att få "
"den här höjden."

#: ../../reference_manual/tools/rectangular_select.rst:61
msgid "Ratio"
msgstr "Förhållande"

#: ../../reference_manual/tools/rectangular_select.rst:61
msgid ""
"Gives the current ratio. Use the lock to force the next selection made to "
"this ratio."
msgstr ""
"Ger det aktuella förhållandet. Använd lås för att tvinga nästa markering att "
"få det här förhållandet."

#: ../../reference_manual/tools/rectangular_select.rst:65
msgid "Round X"
msgstr "Avrunda X"

#: ../../reference_manual/tools/rectangular_select.rst:66
msgid "The horizontal radius of the rectangle corners."
msgstr "Rektangelhörnens horisontella radie."

#: ../../reference_manual/tools/rectangular_select.rst:67
msgid "Round Y"
msgstr "Avrunda Y"

#: ../../reference_manual/tools/rectangular_select.rst:68
msgid "The vertical radius of the rectangle corners."
msgstr "Rektangelhörnens vertikala radie."
