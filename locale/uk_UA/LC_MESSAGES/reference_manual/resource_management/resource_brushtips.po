# Translation of docs_krita_org_reference_manual___resource_management___resource_brushtips.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___resource_management___resource_brushtips\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:49+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../reference_manual/resource_management/resource_brushtips.rst:0
msgid ".. image:: images/brushes/600px-BSE_Predefined_Window.png"
msgstr ".. image:: images/brushes/600px-BSE_Predefined_Window.png"

#: ../../reference_manual/resource_management/resource_brushtips.rst:1
msgid "Managing brush tips in Krita."
msgstr "Керування кінчиками пензлів у Krita."

#: ../../reference_manual/resource_management/resource_brushtips.rst:11
#: ../../reference_manual/resource_management/resource_brushtips.rst:16
msgid "Brushes"
msgstr "Пензлі"

#: ../../reference_manual/resource_management/resource_brushtips.rst:11
msgid "Resources"
msgstr "Ресурси"

#: ../../reference_manual/resource_management/resource_brushtips.rst:11
msgid "Brush Tip"
msgstr "Кінчик пензля"

#: ../../reference_manual/resource_management/resource_brushtips.rst:11
msgid "Brush Mask"
msgstr "Маска пензля"

#: ../../reference_manual/resource_management/resource_brushtips.rst:18
msgid ""
"These are the brush tip or textures used in the brush presets. They can be "
"png files or .abr file from Photoshop or .gbr files from gimp."
msgstr ""
"Це кінчики пензля або текстури, які використовуються у наборах пензлів. Це "
"можуть бути файли png або файл .abr з Photoshop або файли .gbr з Gimp."

#: ../../reference_manual/resource_management/resource_brushtips.rst:22
msgid ""
"Currently Krita only import a brush texture from abr file, you have to "
"recreate the brushes by adding appropriate values in size, spacing etc."
msgstr ""
"У поточній версії Krita може лише імпортувати текстуру пензля з файла abr. "
"Вам доведеться повторно створити пензлі додаванням відповідних значень для "
"розміру, інтервалу тощо."

#: ../../reference_manual/resource_management/resource_brushtips.rst:24
msgid "They can be modified/tagged in the brush preset editor."
msgstr ""
"Їх можна змінювати або встановлювати для них мітки за допомогою редактора "
"наборів пензлів."

#: ../../reference_manual/resource_management/resource_brushtips.rst:26
msgid "See :ref:`option_brush_tip` for more info."
msgstr "Див. :ref:`option_brush_tip`, щоб дізнатися більше."

#: ../../reference_manual/resource_management/resource_brushtips.rst:29
msgid "Example: Loading a Photoshop Brush (\\*.ABR)"
msgstr "Приклад: завантаження пензля Photoshop (\\*.ABR)"

#: ../../reference_manual/resource_management/resource_brushtips.rst:31
msgid ""
"For some time Photoshop has been using the ABR format to compile brushes "
"into a single file.  Krita can read and load .ABR files, although there are "
"certain features. For this example we will use an example of an .ABR file "
"that contains numerous images of types of trees and ferns.  We have two "
"objectives.  The first is to create a series of brushes that we an quickly "
"access from the Brush Presets dock to easily put together a believable "
"forest.  The second is to create a single brush that we can  change on the "
"fly to use for a variety of flora, without the need to have a dedicated "
"Brush Preset for each type."
msgstr ""
"Від певного моменту часу, у Photoshop для збирання пензлів до окремого файла "
"використовують формат ABR. Krita може читати і завантажувати файли .ABR, "
"хоча і з певними особливостями. Для нашого прикладу ми скористаємося файлом ."
"ABR, який містить численні зображення типів дерев і папороті. Маємо дві "
"мети. Першою є створення набору пензлів, доступ до якого для малювання лісу "
"можна буде швидко отримати за допомогою бічної панелі наборів пензлів. "
"Другою метою є створення окремого пензля, який ми зможемо змінювати на льоту "
"для створення враження різноманіття флори без потреби у спеціальному наборі "
"пензлів для кожного типу рослин."

#: ../../reference_manual/resource_management/resource_brushtips.rst:33
msgid ""
"First up is download the file (.ZIP, .RAR,...) that contains the .ABR file "
"and any licensing or other notes.  Be sure to read the license if there is "
"one!"
msgstr ""
"Спочатку слід отримати файл (.ZIP, .RAR,...), який містить файл .ABR і усі "
"умови ліцензування та зауваження. Спочатку, обов'язково ознайомтеся із "
"умовами ліцензування, якщо такі визначено!"

#: ../../reference_manual/resource_management/resource_brushtips.rst:34
msgid "Extract the .ABR file into Krita's home directory for brushes."
msgstr "Розпакуйте файл .ABR до домашнього каталогу Krita для пензлів."

#: ../../reference_manual/resource_management/resource_brushtips.rst:35
msgid ""
"In your Brush Presets dock, select one of your brushes that uses the Pixel "
"Brush Engine.  An Ink Pen or solid fill type should do fine."
msgstr ""
"На бічній панелі наборів пензлів виберіть один з пензлів, для якого "
"використовується рушій піксельних пензлів. Можна вибрати пензель чорнильного "
"пера або суцільного заповнення."

#: ../../reference_manual/resource_management/resource_brushtips.rst:36
msgid "Open the Brush Settings Editor (:kbd:`F5` key)."
msgstr "Відкрийте редактор параметрів пензля (:kbd:`F5`)."

#: ../../reference_manual/resource_management/resource_brushtips.rst:37
msgid ""
"Click on the tab \"Predefined\" next to \"Auto\".  This will change the "
"editor to show a scrollable screen of thumbnail images, most will be black "
"on a white background.  At the bottom of the window are two icons:"
msgstr ""
"Виберіть вкладку «Стандартна», розташовану поряд із вкладкою «Авто». У "
"відповідь у вікні редактора буде показано придатний до гортання список "
"зображень мініатюр, більшість з яких буде чорними на білому тлі. У нижній "
"частині вікна буде розташовано дві піктограми:"

#: ../../reference_manual/resource_management/resource_brushtips.rst:41
msgid ""
"Click on the blue file folder on the left and then navigate to where you "
"saved your .ABR file and open it."
msgstr ""
"Натисніть на піктограми блакитної теки і перейдіть до теки, куди було "
"збережено файл .ABR. Відкрийте файл."

#: ../../reference_manual/resource_management/resource_brushtips.rst:42
msgid ""
"If everything went fine you will see a number of new thumbnails show up at "
"the bottom of the window.  In our case, they would all be thumbnails "
"representing different types of trees.  Your job now is to decide which of "
"these you want to have as Brush Preset (Just like your Pencil) or you think "
"you'll only use sporadically."
msgstr ""
"Якщо програмі вдасться відкрити файл, ви побачите декілька нових мініатюр у "
"нижній частині вікна. У нашому випадку це мініатюри різних типів дерев. "
"Тепер вам слід визначити, які з мініатюр потрібні вам як набори пензлів "
"(подібно до вашого олівця), а які ви використовуватимете лише час від часу."

#: ../../reference_manual/resource_management/resource_brushtips.rst:43
msgid ""
"Let's say that there is an image of an evergreen tree that we're pretty sure "
"is going to be a regular feature in some of our paintings and we want to "
"have a dedicated brush for it.  To do this we would do the following:"
msgstr ""
"Припустімо, серед мініатюр є зображення вічнозеленого дерева, яке, ми "
"впевнені, буде регулярно використовуватися у наших малюнка, тому нам "
"потрібен спеціальний пензель для нього. Для створення такого пензля слід "
"виконати наступні дії:"

#: ../../reference_manual/resource_management/resource_brushtips.rst:44
msgid "Click on the image of the tree we want"
msgstr "Клацніть на потрібному нам зображенні дерева."

#: ../../reference_manual/resource_management/resource_brushtips.rst:45
msgid ""
"Change the name of the brush at the very top of the Brush Editor Settings "
"dialog.  Something like \"Trees - Tall Evergreen\" would be appropriate."
msgstr ""
"Змініть назву пензля у верхній частині вікна параметрів редактора пензлів. "
"Нехай назвою буде «Дерева — високе вічнозелене»."

#: ../../reference_manual/resource_management/resource_brushtips.rst:46
msgid "Click the \"Save to Presets\" button"
msgstr "Натисніть кнопку «Зберегти до наборів»"

#: ../../reference_manual/resource_management/resource_brushtips.rst:47
msgid ""
"Now that you have a \"Tall Evergreen\" brush safely saved you can experiment "
"with the settings to see if there is anything you would like to change, for "
"instance, by altering the size setting and the pressure parameter you could "
"set the brush to change the tree size depending on the pressure you were "
"using with your stylus (assuming you have a stylus!)."
msgstr ""
"Тепер у пензель «Високе вічнозелене» безпечно збережено, і ви можете "
"поекспериментувати з його параметрами, скоригувавши їх до бажаних значень. "
"Наприклад, можна змінити параметр розміру та параметр тиску так, щоб розмір "
"дерева змінювався, залежно від тиску на стило (якщо ви користуєтеся для "
"малювання стилом!)."

#: ../../reference_manual/resource_management/resource_brushtips.rst:48
msgid ""
"Once you're satisfied with your brush and its settings you need to do one "
"last thing (but click :guilabel:`Overwrite Brush` first!)"
msgstr ""
"Щойно буде визначено бажані параметри пензля, вам слід зробити останню річ "
"(але натисніть спочатку кнопку :guilabel:`Перезаписати пензель`!)"

#: ../../reference_manual/resource_management/resource_brushtips.rst:50
msgid ""
"It's time now to create the Brush Preview graphic. The simplest and easiest "
"way to do this for a brush of this type is to clear out the ScratchPad using "
"the :guilabel:`Reset` button. Now, center your cursor in the Brush Preview "
"square at the top of the ScratchPad and click once. You should see an image "
"of your texture (in this case it would be the evergreen tree). In order to "
"work correctly though the entire image should fit comfortably within the "
"square. This might mean that you have to tweak the size of the brush. Once "
"you have something you are happy with then click the :guilabel:`Overwrite "
"Brush` button and your brush and its preview image will be saved."
msgstr ""
"Час створити графіку для попереднього перегляду пензля. Найпростішим "
"способом для пензлів цього типу є спорожнення зображення на панелі нотатника "
"за допомогою кнопки :guilabel:`Скинути`. Далі, слід розташування курсор у "
"центрі квадрата панелі нотатника і клацнути лівою кнопкою миші. Ви маєте "
"побачити зображення вашої текстури (у нашому випадку це буде вічнозелене "
"дерево). Втім, щоб це спрацювало, зображення має вміщатися до нашого "
"квадрата. Це означає, що, можливо, доведеться скоригувати розмір пензля. "
"Щойно ви досягнете бажаного результату, можна натиснути кнопку :guilabel:"
"`Перезаписати пензель`, і програма збереже ваш пензель і зображення "
"попереднього перегляду для нього."

#: ../../reference_manual/resource_management/resource_brushtips.rst:52
msgid ""
"An alternative method that requires a little more work but gives you greater "
"control of the outcome is the following:"
msgstr ""
"Альтернативним методом, який потребуватиме дещо більше зусиль, але надасть "
"вам ширший контроль над результатом, є такий:"

#: ../../reference_manual/resource_management/resource_brushtips.rst:54
msgid ""
"Locate the Brush Preview thumbnail .KPP file in Krita and open it to get a "
"200x200 file that you can edit to your wishes."
msgstr ""
"Встановіть розташування файла мініатюри попереднього перегляду пензля .KPP у "
"Krita і відкрийте цей файл розміром 200x200 пікселів для редагування."

#: ../../reference_manual/resource_management/resource_brushtips.rst:56
msgid ""
"You're ready to add the next texture!  From here on it's just a matter of "
"wash, rinse and repeat for each texture where you want to create a dedicated "
"Brush Preset."
msgstr ""
"Ви готові до додавання наступної текстури! З цього моменту, достатньо "
"вичистити, виправити і повторити дії з кожною текстурою, для якої ви хочете "
"створити відповідний набір пензлів."
