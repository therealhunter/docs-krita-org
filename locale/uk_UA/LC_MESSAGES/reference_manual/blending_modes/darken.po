# Translation of docs_krita_org_reference_manual___blending_modes___darken.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___blending_modes___darken\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 08:23+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../reference_manual/blending_modes/darken.rst:1
msgid ""
"Page about the darken blending modes in Krita: Darken, Burn, Easy Burn, Fog "
"Darken, Darker Color, Gamma Dark, Linear Burn and Shade."
msgstr ""
"торінка, яку присвячено режимам злиття із затемненням у Krita: затемнення, "
"вигоряння, просте вигоряння, туманне затемнення, затемнення кольору, "
"затемнення гами, лінійне вигоряння та затінення."

#: ../../reference_manual/blending_modes/darken.rst:16
#: ../../reference_manual/blending_modes/darken.rst:75
#: ../../reference_manual/blending_modes/darken.rst:79
msgid "Darken"
msgstr "Затемнення"

#: ../../reference_manual/blending_modes/darken.rst:18
msgid "Color Burn"
msgstr "Вигоряння кольорів"

#: ../../reference_manual/blending_modes/darken.rst:18
msgid "burn"
msgstr "вигоряння"

#: ../../reference_manual/blending_modes/darken.rst:23
msgid "Burn"
msgstr "Вигоряння"

#: ../../reference_manual/blending_modes/darken.rst:25
msgid "A variation on Divide, sometimes called 'Color Burn' in some programs."
msgstr ""
"Різновид «Ділення», іноді називається «Вигоряння кольору» у деяких програмах."

#: ../../reference_manual/blending_modes/darken.rst:27
msgid ""
"This inverts the bottom layer, then divides it by the top layer, and inverts "
"the result. This results in a darkened effect that takes the colors of the "
"lower layer into account, similar to the burn technique used in traditional "
"darkroom photography."
msgstr ""
"Інвертує нижній шар, ділить його на верхній шар і інвертує результат. Цей "
"призводить до ефекту потемнішання із врахуванням кольорів нижнього шару, "
"подібно до методики ефекту вигоряння у традиційному фотомистецтві."

#: ../../reference_manual/blending_modes/darken.rst:30
msgid ""
"1_{[1_Darker Gray(0.4, 0.4, 0.4)] / Lighter Gray(0.5, 0.5, 0.5)} = (-0.2, "
"-0.2, -0.2) → Black(0, 0, 0)"
msgstr ""
"1_{[1_Темно-сірий(0.4, 0.4, 0.4)] / Світло-сірий(0.5, 0.5, 0.5)} = (-0.2, "
"-0.2, -0.2) → Чорний(0, 0, 0)"

#: ../../reference_manual/blending_modes/darken.rst:35
msgid ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Burn_Gray_0.4_and_Gray_0.5_n.png"
msgstr ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Burn_Gray_0.4_and_Gray_0.5_n.png"

#: ../../reference_manual/blending_modes/darken.rst:35
#: ../../reference_manual/blending_modes/darken.rst:42
#: ../../reference_manual/blending_modes/darken.rst:47
msgid "Left: **Normal**. Right: **Burn**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Вигоряння**."

#: ../../reference_manual/blending_modes/darken.rst:37
msgid ""
"1_{[1_Light Blue(0.1608, 0.6274, 0.8274)] / Orange(1, 0.5961, 0.0706)} = "
"(0.1608, 0.3749, -1.4448) → Green(0.1608, 0.3749, 0)"
msgstr ""
"1_{[1_Світло-синій(0.1608, 0.6274, 0.8274)] / Помаранчевий(1, 0.5961, "
"0.0706)} = (0.1608, 0.3749, -1.4448) → Зелений(0.1608, 0.3749, 0)"

#: ../../reference_manual/blending_modes/darken.rst:42
msgid ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Burn_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Burn_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/darken.rst:47
msgid ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Burn_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Burn_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/darken.rst:49
#: ../../reference_manual/blending_modes/darken.rst:53
msgid "Easy Burn"
msgstr "Просте вигоряння"

#: ../../reference_manual/blending_modes/darken.rst:55
msgid ""
"Aims to solve issues with Color Burn blending mode by using a formula which "
"falloff is similar to Dodge, but the falloff rate is softer. It is within "
"the range of 0.0f and 1.0f unlike Color Burn mode."
msgstr ""
"Створений для усування вад режиму змішування «Вигоряння кольорів» шляхом "
"використання формули, результат застосування якої близький до «Висвітлення, "
"але швидкість спадання є меншою. Кольори результату потрапляють до проміжку "
"від 0.0f до 1.0f, на відміну від режиму «Вигоряння кольорів»."

#: ../../reference_manual/blending_modes/darken.rst:60
msgid ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Easy_Burn_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Easy_Burn_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/darken.rst:60
msgid "Left: **Normal**. Right: **Easy Burn**"
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Просте вигоряння**."

#: ../../reference_manual/blending_modes/darken.rst:62
msgid "Fog Darken"
msgstr "Туманне затемнення"

#: ../../reference_manual/blending_modes/darken.rst:66
msgid "Fog Darken (IFS Illusions)"
msgstr "Туманне затемнення (IFS-ілюзії)"

#: ../../reference_manual/blending_modes/darken.rst:68
msgid ""
"Darken the image in a way that there is a 'fog' in the end result. This is "
"due to the unique property of fog darken in which midtones combined are "
"lighter than non-midtones blend."
msgstr ""
"Затемнює зображення так, що у кінцевому результаті стає помітним «туман». "
"Причиною появи туману є унікальна властивість туманного затемнення, яка "
"полягає у тому, що поєднання середніх тонів є світлішим за суміш тонів, які "
"не є середніми."

#: ../../reference_manual/blending_modes/darken.rst:73
msgid ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Fog_Darken_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Fog_Darken_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/darken.rst:73
msgid "Left: **Normal**. Right: **Fog Darken** (exactly the same as Addition)."
msgstr ""
"Ліворуч: **звичайне змішування**. Праворуч: **туманне затемнення** (те саме, "
"що і «Додавання»)."

#: ../../reference_manual/blending_modes/darken.rst:81
msgid ""
"With the darken, the upper layer's colors are checked for their lightness. "
"Only if they are darker than the underlying color on the lower layer, will "
"they be visible."
msgstr ""
"При застосуванні ефекту потемнішання виконується перевірка освітленості "
"кольорів верхнього шару. Ці кольори будуть видимими, лише якщо вони є "
"темнішими за кольори нижнього шару."

#: ../../reference_manual/blending_modes/darken.rst:83
msgid ""
"Is Lighter Gray(0.5, 0.5, 0.5) darker than Darker Gray(0.4, 0.4, 0.4)? = "
"(no, no, no) → Darker Gray(0.4, 0.4, 0.4)"
msgstr ""
"Чи Світло-сірий(0.5, 0.5, 0.5) темніший за Темно-сірий(0.4, 0.4, 0.4)? = "
"(ні, ні, ні) → Темно-сірий(0.4, 0.4, 0.4)"

#: ../../reference_manual/blending_modes/darken.rst:88
msgid ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Darken_Gray_0.4_and_Gray_0.5_n.png"
msgstr ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Darken_Gray_0.4_and_Gray_0.5_n.png"

#: ../../reference_manual/blending_modes/darken.rst:88
#: ../../reference_manual/blending_modes/darken.rst:95
#: ../../reference_manual/blending_modes/darken.rst:100
msgid "Left: **Normal**. Right: **Darken**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Затемнення**."

#: ../../reference_manual/blending_modes/darken.rst:90
msgid ""
"Is Orange(1, 0.5961, 0.0706) darker than Light Blue(0.1608, 0.6274, 0.8274)? "
"= (no, yes, yes) → Green(0.1608, 0.5961, 0.0706)"
msgstr ""
"Чи Помаранчевий(1, 0.5961, 0.0706) темніший за Світло-синій(0.1608, 0.6274, "
"0.8274)? = (ні, так, так) → Зелений(0.1608, 0.5961, 0.0706)"

#: ../../reference_manual/blending_modes/darken.rst:95
msgid ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Darken_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Darken_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/darken.rst:100
msgid ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Darken_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Darken_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/darken.rst:102
#: ../../reference_manual/blending_modes/darken.rst:106
msgid "Darker Color"
msgstr "Затемнення кольору"

#: ../../reference_manual/blending_modes/darken.rst:111
msgid ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Darker_Color_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Darker_Color_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/darken.rst:111
msgid "Left: **Normal**. Right: **Darker Color**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Затемнення кольору**."

#: ../../reference_manual/blending_modes/darken.rst:113
#: ../../reference_manual/blending_modes/darken.rst:117
msgid "Gamma Dark"
msgstr "Затемнення гами"

#: ../../reference_manual/blending_modes/darken.rst:119
msgid ""
"Divides 1 by the upper layer, and calculates the end result using that as "
"the power of the lower layer."
msgstr ""
"Ділить 1 на значення кольору верхнього шару і обчислює відповідний степінь "
"значення у нижньому шарі."

#: ../../reference_manual/blending_modes/darken.rst:121
msgid ""
"Darker Gray(0.4, 0.4, 0.4)^[1 / Lighter Gray(0.5, 0.5, 0.5)] = Even Darker "
"Gray(0.1600, 0.1600, 0.1600)"
msgstr ""
"Темно-сірий(0.4, 0.4, 0.4)^[1 / Світло-сірий(0.5, 0.5, 0.5)] = Ще-темніший-"
"сірий(0.1600, 0.1600, 0.1600)"

#: ../../reference_manual/blending_modes/darken.rst:126
msgid ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Gamma_Dark_Gray_0.4_and_Gray_0.5_n.png"
msgstr ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Gamma_Dark_Gray_0.4_and_Gray_0.5_n.png"

#: ../../reference_manual/blending_modes/darken.rst:126
#: ../../reference_manual/blending_modes/darken.rst:133
#: ../../reference_manual/blending_modes/darken.rst:138
msgid "Left: **Normal**. Right: **Gamma Dark**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Затемнення гами**."

#: ../../reference_manual/blending_modes/darken.rst:128
msgid ""
"Light Blue(0.1608, 0.6274, 0.8274)^[1 / Orange(1, 0.5961, 0.0706)] = "
"Green(0.1608, 0.4575, 0.0683)"
msgstr ""
"Світло-синій(0.1608, 0.6274, 0.8274)^[1 / Помаранчевий(1, 0.5961, 0.0706)] = "
"Зелений(0.1608, 0.4575, 0.0683)"

#: ../../reference_manual/blending_modes/darken.rst:133
msgid ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Gamma_Dark_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Gamma_Dark_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/darken.rst:138
msgid ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Gamma_Dark_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Gamma_Dark_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/darken.rst:140
#: ../../reference_manual/blending_modes/darken.rst:144
msgid "Linear Burn"
msgstr "Лінійне вигоряння"

#: ../../reference_manual/blending_modes/darken.rst:146
msgid ""
"Adds the values of the two layers together and then subtracts 1. Seems to "
"produce the same result as :ref:`bm_inverse_subtract`."
msgstr ""
"Обчислює суму значень у двох шарах і віднімає від отриманого значення 1. "
"Здається, дає той самий результат, що і :ref:`bm_inverse_subtract`."

#: ../../reference_manual/blending_modes/darken.rst:148
msgid ""
"[Darker Gray(0.4, 0.4, 0.4) + Lighter Gray(0.5, 0.5, 0.5)]_1 = (-0.1000, "
"-0.1000, -0.1000)  → Black(0, 0, 0)"
msgstr ""
"[Темно-сірий(0.4, 0.4, 0.4) + Світло-сірий(0.5, 0.5, 0.5)]_1 = (-0.1000, "
"-0.1000, -0.1000) → Чорний(0, 0, 0)"

#: ../../reference_manual/blending_modes/darken.rst:153
msgid ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Linear_Burn_Gray_0.4_and_Gray_0.5.png"
msgstr ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Linear_Burn_Gray_0.4_and_Gray_0.5.png"

#: ../../reference_manual/blending_modes/darken.rst:153
#: ../../reference_manual/blending_modes/darken.rst:160
#: ../../reference_manual/blending_modes/darken.rst:165
msgid "Left: **Normal**. Right: **Linear Burn**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Лінійне вигоряння**."

#: ../../reference_manual/blending_modes/darken.rst:155
msgid ""
"[Light Blue(0.1608, 0.6274, 0.8274) + Orange(1, 0.5961, 0.0706)]_1 = "
"(0.1608, 0.2235, -0.1020) → Dark Green(0.1608, 0.2235, 0)"
msgstr ""
"[Світло-синій(0.1608, 0.6274, 0.8274) + Помаранчевий(1, 0.5961, 0.0706)]_1 = "
"(0.1608, 0.2235, -0.1020) → Темно-зелений(0.1608, 0.2235, 0)"

#: ../../reference_manual/blending_modes/darken.rst:160
msgid ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Linear_Burn_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Linear_Burn_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/darken.rst:165
msgid ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Linear_Burn_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Linear_Burn_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/darken.rst:167
msgid "Shade"
msgstr "Тінь"

#: ../../reference_manual/blending_modes/darken.rst:171
msgid "Shade (IFS Illusions)"
msgstr "Тінь (IFS-ілюзії)"

#: ../../reference_manual/blending_modes/darken.rst:173
msgid ""
"Basically, the blending mode only ends in shades of shades. This means that "
"it's very useful for painting shading colors while still in the range of "
"shades."
msgstr ""
"На базовому рівні, цей режим змішування лише додає відтінки до відтінків. Це "
"означає, що він є дуже корисним для малювання кольорів певного відтінку у "
"межах певного діапазону відтінків."

#: ../../reference_manual/blending_modes/darken.rst:179
msgid ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Shade_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/darken/"
"Blending_modes_Shade_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/darken.rst:179
msgid "Left: **Normal**. Right: **Shade**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Тінь**."
