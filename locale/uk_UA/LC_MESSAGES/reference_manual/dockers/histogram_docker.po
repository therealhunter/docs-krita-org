# Translation of docs_krita_org_reference_manual___dockers___histogram_docker.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___dockers___histogram_docker\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-06-11 08:25+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../reference_manual/dockers/histogram_docker.rst:1
msgid "Overview of the histogram docker."
msgstr "Огляд бічної панелі гістограми."

#: ../../reference_manual/dockers/histogram_docker.rst:11
msgid "Histogram"
msgstr "Гістограма"

#: ../../reference_manual/dockers/histogram_docker.rst:16
msgid "Histogram Docker"
msgstr "Бічна панель гістограми"

#: ../../reference_manual/dockers/histogram_docker.rst:18
msgid ""
"A Histogram is a chart that shows how much of a specific channel value is "
"used in an image. Its purpose is to give a really technical representation "
"of the colors in an image, which can be helpful in decision making about "
"filters."
msgstr ""
"Гістограма — діаграма часток зображення, на яких використано певне значення "
"каналу. Її призначено для реального технічного представлення кольорів на "
"зображенні. Гістограма може бути корисною для визначення фільтрів, які варто "
"застосувати до зображення."

#: ../../reference_manual/dockers/histogram_docker.rst:21
msgid ".. image:: images/dockers/Histogram_docker.png"
msgstr ".. image:: images/dockers/Histogram_docker.png"

#: ../../reference_manual/dockers/histogram_docker.rst:22
msgid ""
"The histogram docker was already available via :menuselection:`Layers --> "
"Histogram`, but it's now a proper docker."
msgstr ""
"Доступ до бічної панелі гістограми можна отримати за допомогою пункту меню :"
"menuselection:`Шари --> Гістограма`, тепер є окрема бічна панель."

#: ../../reference_manual/dockers/histogram_docker.rst:24
msgid "External Links:"
msgstr "Зовнішні посилання:"

#: ../../reference_manual/dockers/histogram_docker.rst:26
msgid ""
"`Wikipedia's entry on image histograms. <https://en.wikipedia.org/wiki/"
"Image_histogram>`_"
msgstr ""
"`Стаття у Вікіпедії щодо гістограм зображень <https://en.wikipedia.org/wiki/"
"Image_histogram>`_"
