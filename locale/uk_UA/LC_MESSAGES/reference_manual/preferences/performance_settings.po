# Translation of docs_krita_org_reference_manual___preferences___performance_settings.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___preferences___performance_settings\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 15:06+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../<generated>:1
msgid "Enable Background Cache Generation"
msgstr "Увімкнути створення кешу у фоновому режимі"

#: ../../reference_manual/preferences/performance_settings.rst:1
msgid "Performance settings in Krita."
msgstr "Параметри швидкодії у Krita."

#: ../../reference_manual/preferences/performance_settings.rst:11
#: ../../reference_manual/preferences/performance_settings.rst:23
msgid "RAM"
msgstr "Пам’ять"

#: ../../reference_manual/preferences/performance_settings.rst:11
#: ../../reference_manual/preferences/performance_settings.rst:57
msgid "Multithreading"
msgstr "Багатопотоковість"

#: ../../reference_manual/preferences/performance_settings.rst:11
msgid "Preferences"
msgstr "Налаштування"

#: ../../reference_manual/preferences/performance_settings.rst:11
msgid "Settings"
msgstr "Параметри"

#: ../../reference_manual/preferences/performance_settings.rst:11
msgid "Performance"
msgstr "Швидкодія"

#: ../../reference_manual/preferences/performance_settings.rst:11
msgid "Memory Usage"
msgstr "Використання пам'яті"

#: ../../reference_manual/preferences/performance_settings.rst:11
msgid "Lag"
msgstr "Затримка"

#: ../../reference_manual/preferences/performance_settings.rst:16
msgid "Performance Settings"
msgstr "Параметри швидкодії"

#: ../../reference_manual/preferences/performance_settings.rst:18
msgid ""
":program:`Krita`, as a painting program, juggles a lot of data around, like "
"the brushes you use, the colors you picked, but primarily, each pixel in "
"your image. Due to this, how :program:`Krita` organizes where it stores all "
"the data can really speed up :program:`Krita` while painting, just like "
"having an organized artist's workplace can really speed up the painting "
"process in real life."
msgstr ""
":program:`Krita`, як програма для малювання, працює із великим обсягами "
"даних, зокрема даними щодо використаних пензлів, вибраних кольорів, але, в "
"основному, з усіма пікселями зображення. Через це, правильне визначення умов "
"зберігання даних у :program:`Krita` може значно пришвидшити роботу :program:"
"`Krita` під час малювання, так само, як правильна організація робочого місця "
"художника може пришвидшити процес традиційного малювання."

#: ../../reference_manual/preferences/performance_settings.rst:20
msgid ""
"These preferences allow you to configure :program:`Krita's` organisation, "
"but all do require you to restart :program:`Krita`, so it can do this "
"organisation properly."
msgstr ""
"Ці налаштування надають вам змогу змінити організацію самої :program:"
"`Krita`, але потребують перезапуску :program:`Krita` для належного "
"застосування зміненої організації."

#: ../../reference_manual/preferences/performance_settings.rst:25
msgid ""
"RAM, or Random Access Memory, is the memory your computer is immediately "
"using. The difference between RAM and the hard drive memory can be compared "
"to the difference between having files on your desk and having files safely "
"stored away in an archiving room: The files on your desk as much easier to "
"access than the ones in your archive, and it takes time to pull new files "
"from the archive. This is the same for your computer and RAM. Files need to "
"be loaded into RAM before the computer can really use them, and storing and "
"removing them from RAM takes time."
msgstr ""
"RAM або Random Access Memory (пам'ять із випадковим доступом) — оперативна "
"пам'ять вашого комп'ютера, якою той може безпосередньо користуватися. "
"Відмінність між RAM та пам'яттю на диску подібна до відмінності між теками, "
"які можуть лежати на вашому столі, і теками у архіві: до тек на столі "
"набагато простіше отримати доступ, ніж до тек в архіві; а для того, щоб "
"отримати теки з архіву завжди потрібен певний час. Те саме можна сказати і "
"про ваш комп'ютер і RAM. Файли слід завантажити до оперативної пам'яті, перш "
"ніж комп'ютер зможе ними користуватися, а збереження і вилучення файлів з "
"RAM потребує певного часу."

#: ../../reference_manual/preferences/performance_settings.rst:27
msgid ""
"These settings allow you to choose how much of your virtual desk you "
"dedicate to :program:`Krita`. :program:`Krita` will then reserve them on "
"start-up. This does mean that if you change any of the given options, you "
"need to restart :program:`Krita` so it can make this reservation."
msgstr ""
"За допомогою цих параметрів ви можете визначити, скільки пам'яті на "
"віртуальному диску буде віддано :program:`Krita`. :program:`Krita` "
"зарезервує цей об'єм пам'яті при запуску. Це означає, що якщо ви внесете "
"зміни до будь-якого з параметрів, вам доведеться перезапустити :program:"
"`Krita`, щоб оновити резервування пам'яті."

#: ../../reference_manual/preferences/performance_settings.rst:29
msgid "Memory Limit"
msgstr "Обмеження пам’яті"

#: ../../reference_manual/preferences/performance_settings.rst:30
msgid ""
"This is the maximum space :program:`Krita` will reserve on your RAM on "
"startup. It's both available in percentages and Bytes, so you can specify "
"precisely. :program:`Krita` will not take up more space than this, making it "
"safe for you to run an internet browser or music on the background."
msgstr ""
"Це максимальний об'єм оперативної пам'яті, який :program:`Krita` "
"резервуватиме після запуску. Об'єм можна вказувати у відсотках або байтах. "
"Отже, ви можете вказати його точно. :program:`Krita` не забиратиме більше "
"місця, ніж вказано. Це надасть вам змогу безпечно запускати програму для "
"перегляду інтернету або музичний програвач у фоновому режимі."

#: ../../reference_manual/preferences/performance_settings.rst:32
msgid ""
"A feature for advanced computer users. This allows :program:`Krita` to "
"organize the area it takes up on the virtual working desk before putting its "
"data on there. Like how a painter has a standard spot for their canvas, :"
"program:`Krita` also benefits from giving certain data it uses its place (a "
"memory pool), so that it can find them easily, and it doesn't get lost among "
"the other data (memory fragmentation). It will then also not have to spend "
"time finding a spot for this data."
msgstr ""
"Можливість для досвідчених користувачів комп'ютера. За її допомогою ви "
"можете дозволити :program:`Krita` упорядкувати область, яку програма "
"використовує на своєму віртуальному мольберті, перш ніж розміщувати у ній "
"дані. Подібно до художника, який має стандартне місце для полотна, :program:"
"`Krita` може краще працювати, якщо розташовує певні дані у відповідному "
"місці (буфері пам'яті), щоб їх простіше було знайти і не втратити серед "
"інших даних (внаслідок фрагментації пам'яті). Це також надає змогу програмі "
"не витрачати зайвий час на пошук цих даних."

#: ../../reference_manual/preferences/performance_settings.rst:34
msgid ""
"Increasing this, of course, means there's more space for this type of data, "
"but like how filling up your working desk with only one big canvas will make "
"it difficult to find room for your paints and brushes, having a large "
"internal pool will result in :program:`Krita` not knowing where to put the "
"other non-specific data."
msgstr ""
"Збільшення цього значення, звичайно ж, означає розширення обсягу пам'яті для "
"цього типу даних, але, подібно до того, як розташування великого полотна на "
"робочому столі лишає мало місця для ваших фарб і пензлів, збільшення "
"внутрішнього буфера може призвести до того, :program:`Krita` буде важко "
"розташувати у пам'яті інші неспецифічні дані."

#: ../../reference_manual/preferences/performance_settings.rst:36
msgid ""
"On the opposite end, not giving your canvas a spot at all, will result in "
"you spending more time looking for a place where you will put the new layer "
"or that reference you just took out of the storage. This happens for :"
"program:`Krita` as well, making it slower."
msgstr ""
"З іншого боку, якщо не надати вашому полотну достатнього місця, вам "
"доведеться витрачати більше часу на спроби розташувати його так, щоб ви "
"могли нанести новий шар фарби, або пошук частини полотна, яку щойно було "
"знято зі стола і покладено до сховища. Те саме стосується і :program:"
"`Krita`: нестача місця уповільнює роботу програми."

#: ../../reference_manual/preferences/performance_settings.rst:38
msgid ""
"This is recommended to be a size of one layer of your image, e.g. if you "
"usually paint on the image of 3000x3000x8bit-ARGB, the pool should be "
"something like 36 MiB."
msgstr ""
"Рекомендованим значенням є розмір одного шару вашого зображення. Наприклад, "
"якщо ви малюєте зображення 3000x3000x8-бітів-ARGB, буфер повинен бути "
"розміром приблизно 36 МіБ."

#: ../../reference_manual/preferences/performance_settings.rst:39
msgid "Internal Pool"
msgstr "Внутрішній буфер"

#: ../../reference_manual/preferences/performance_settings.rst:40
msgid ""
"As :program:`Krita` does this on start-up, you will need to restart :program:"
"`Krita` to have this change affect anything."
msgstr ""
"Оскільки :program:`Krita` виконує цю дію під час запуску, вам доведеться "
"перезапустити :program:`Krita`, щоб ця зміна набула чинності."

#: ../../reference_manual/preferences/performance_settings.rst:42
msgid ""
":program:`Krita` also needs to keep all the Undo states on the virtual desk "
"(RAM). Swapping means that parts of the files on the virtual desk get sent "
"to the virtual archive room. This allows :program:`Krita` to dedicate more "
"RAM space to new actions, by sending old Undo states to the archive room "
"once it hits this limit. This will make undoing a little slower, but this "
"can be desirable for the performance of :program:`Krita` overall. This too "
"needs :program:`Krita` to be restarted."
msgstr ""
"Крім того, :program:`Krita` потрібно місце для збереження усіх станів "
"скасування у оперативній пам'яті (RAM). Використання резервної пам'яті "
"означає, що частину тек на віртуальному столі буде надіслано до віртуального "
"архіву. Це надасть змогу :program:`Krita` використовувати більше обсягу RAM "
"для нових дій, надіславши дані щодо застарілих станів зображення до архіву, "
"щойно буде перевищено вказане вами обмеження. Це дещо уповільнює "
"скасовування дій, але може бути бажаним для загального підвищення швидкодії :"
"program:`Krita`. Застосування нового значення параметра потребує "
"перезапуску :program:`Krita`."

#: ../../reference_manual/preferences/performance_settings.rst:43
msgid "Swap Undo After"
msgstr "Буфер скасовування до резервної пам’яті після"

#: ../../reference_manual/preferences/performance_settings.rst:46
msgid "Swapping"
msgstr "Резервна пам'ять на диску"

#: ../../reference_manual/preferences/performance_settings.rst:48
msgid "File Size Limit"
msgstr "Обмеження на розмір файла"

#: ../../reference_manual/preferences/performance_settings.rst:49
msgid ""
"This determines the limit of the total space :program:`Krita` can take up in "
"the virtual archive room. If :program:`Krita` hits the limit of both the "
"memory limit above, and this Swap File limit, it can't do anything anymore "
"(and will freeze)."
msgstr ""
"Цей параметр визначає обмеження загального обсягу пам'яті, яку :program:"
"`Krita` може використовувати для архівування даних. Якщо :program:`Krita` "
"перевищить обмеження за обсягом пам'яті, встановлене вище, та це обмеження "
"на файл резервної пам'яті, програма не зможе виконувати ніяких дій (і просто "
"«замерзне»)."

#: ../../reference_manual/preferences/performance_settings.rst:51
msgid "Swap File Location"
msgstr "Розташування файла резервної пам’яті"

#: ../../reference_manual/preferences/performance_settings.rst:51
msgid ""
"This determines where the Swap File will be stored on your hard-drive. "
"Location can make a difference, for example, Solid State Drives (SSD) are "
"faster than Hard Disk Drives (HDD). Some people even like to use USB-sticks "
"for the swap file location."
msgstr ""
"За допомогою цього пункту можна визначити місце зберігання файла резервної "
"пам'яті на диску вашого комп'ютера. Це місце зберігання може значно вплинути "
"на роботу програми. Наприклад, твердотільні диски (SSD) працюють швидше за "
"звичайні диски (HDD). Дехто навіть використовує для файлів резервної пам'яті "
"флешки USB."

#: ../../reference_manual/preferences/performance_settings.rst:54
msgid "Advanced"
msgstr "Додатково"

#: ../../reference_manual/preferences/performance_settings.rst:59
msgid ""
"Since 4.0, Krita supports multithreading for the animation cache and "
"handling the drawing of brush tips when using the pixel brush."
msgstr ""
"Починаючи з версії 4.0, у Krita передбачено багатопотокову обробку для кешу "
"анімації та малювання кінчиків пензлів при використанні піксельного пензля."

#: ../../reference_manual/preferences/performance_settings.rst:61
msgid "CPU Limit"
msgstr "Обмеження часу процесора"

#: ../../reference_manual/preferences/performance_settings.rst:62
msgid "The number of cores you want to allow Krita to use when multithreading."
msgstr ""
"Кількість ядер процесора, які дозволено використовувати Krita у режимі "
"багатопотокової обробки."

#: ../../reference_manual/preferences/performance_settings.rst:64
msgid "Frame Rendering Clones Limit"
msgstr "Обмеження клонів під час обробки кадрів"

#: ../../reference_manual/preferences/performance_settings.rst:64
msgid ""
"When rendering animations to frames, Krita multithreads by keeping a few "
"copies of the image, with a maximum determined by the number of cores your "
"processor has. If you have a heavy animation file and lots of cores, the "
"copies can be quite heavy on your machine, so in that case try lowering this "
"value."
msgstr ""
"При обробці кадрів анімації Krita працює у декілька потоків, зберігаючи у "
"пам'яті декілька копій зображення. Максимальна кількість цих копій "
"визначається за кількістю ядер процесора вашого комп'ютера. Якщо у вас є "
"складний до обробки файл анімації і багатоядерний процесор, для зберігання "
"копій у пам'яті може знадобитися доволі багато місця, тому у цьому випадку "
"вам слід зменшити значення параметра у цьому пункті."

#: ../../reference_manual/preferences/performance_settings.rst:67
msgid "Other"
msgstr "Інший"

#: ../../reference_manual/preferences/performance_settings.rst:68
msgid "Limit frames per second while painting."
msgstr "Обмежити кількість кадрів за секунду під час малювання"

#: ../../reference_manual/preferences/performance_settings.rst:69
msgid ""
"This makes the canvas update less often, which means Krita can spend more "
"time calculating other things. Some people find fewer updates unnerving to "
"watch however, hence this is configurable."
msgstr ""
"За допомогою цього пункту можна наказати програмі рідше оновлювати "
"зображення полотна, отже Krita зможе витрачати більше процесорного часу на "
"інші обчислення. Втім, дехто нервується через рідші оновлення, тому ми "
"зробили цей параметр доступним до налаштовування."

#: ../../reference_manual/preferences/performance_settings.rst:70
msgid "Debug logging of OpenGL framerate"
msgstr "Діагностичне журналювання частоти кадрів OpenGL"

#: ../../reference_manual/preferences/performance_settings.rst:71
msgid "Will show the canvas framerate on the canvas when active."
msgstr "Якщо позначено, показувати частоту кадрів на полотні."

#: ../../reference_manual/preferences/performance_settings.rst:72
msgid "Debug logging for brush rendering speed."
msgstr "Діагностичне журналювання швидкості показу пензля/"

#: ../../reference_manual/preferences/performance_settings.rst:73
msgid ""
"Will show numbers indicating how fast the last brush stroke was on canvas."
msgstr ""
"Якщо позначено, показувати числа, які відповідають швидкості останнього "
"штриха пензлем на полотні."

#: ../../reference_manual/preferences/performance_settings.rst:74
msgid "Disable vector optimizations (for AMD CPUs)"
msgstr "Вимкнути векторні оптимізації (для процесорів AMD)"

#: ../../reference_manual/preferences/performance_settings.rst:75
msgid ""
"Vector optimizations are a special way of asking the CPU to do maths, these "
"have names such as SIMD and AVX. These optimizations can make Krita a lot "
"faster when painting, except when you have an AMD CPU under Windows. There "
"seems to be something strange going on there, so just deactivate them then."
msgstr ""
"Векторні оптимізації — особливий спосіб виконання математичних дій "
"процесором. Векторні оптимізації мають власні назви, зокрема SIMD і AVX. Ці "
"оптимізації можуть значно пришвидшити роботу Krita під час малювання, окрім "
"випадків використання процесорів AMD у Windows. На цих процесорах все "
"відбувається якось дивно, тому для них ці оптимізації слід вимкнути."

#: ../../reference_manual/preferences/performance_settings.rst:76
msgid "Enable progress reporting"
msgstr "Увімкнути звітування щодо поступу"

#: ../../reference_manual/preferences/performance_settings.rst:77
msgid ""
"This allows you to toggle the progress reporter, which is a little feedback "
"progress bar that shows up in the status bar when you let Krita do heavy "
"operations, such as heavy filters or big strokes. The red icon next to the "
"bar will allow you to cancel your operation. This is on by default, but as "
"progress reporting itself can take up some time, you can switch it off here."
msgstr ""
"За допомогою цього пункту ви можете увімкнути або вимкнути звітування щодо "
"поступу, тобто невеличку смужку поступу, яку буде показано на смужці стану "
"вікна програми і яку Krita використовуватиме під час виконання довготривалих "
"дій, зокрема застосування складних фільтрів або малювання великих мазків, "
"для сповіщення користувача про поступ дії. За допомогою червоної піктограми, "
"розташованої поруч із смужкою поступу, ви зможете скасувати дію. Цей пункт "
"типово позначено, але оскільки звітування щодо поступу теж потребує "
"процесорного часу, ви можете вимкнути показ смужки поступу і дещо "
"пришвидшити роботу програми."

#: ../../reference_manual/preferences/performance_settings.rst:79
msgid ""
"This enables performance logging, which is then saved to the ``Log`` folder "
"in your ``working directory``. Your working directory is where the autosave "
"is saved at as well."
msgstr ""
"Цей пункт керує журналюванням швидкодії. Програма зберігає дані до теки "
"``Log`` у вашому ``робочому каталозі``. Робочий каталог — це каталог, де "
"зберігаються також і автоматичні резервні копії."

#: ../../reference_manual/preferences/performance_settings.rst:81
msgid "Performance logging"
msgstr "Журналювання швидкодії"

#: ../../reference_manual/preferences/performance_settings.rst:81
msgid ""
"So for unnamed files, this is the ``$HOME`` folder in Linux, and the ``%TEMP"
"%`` folder in Windows."
msgstr ""
"Отже, для файлів без назви це така ``$HOME`` у Linux і тека ``%TEMP%`` у "
"Windows."

#: ../../reference_manual/preferences/performance_settings.rst:84
msgid "Animation Cache"
msgstr "Кеш анімації"

#: ../../reference_manual/preferences/performance_settings.rst:88
msgid ""
"The animation cache is the space taken up by animation frames in the memory "
"of the computer. A cache in this sense is a cache of precalculated images."
msgstr ""
"Кеш анімації це область пам'яті, яку займають кадри анімації у пам'яті "
"комп'ютера. Кеш, у певному сенсі, є кешем попередньо оброблених зображень."

#: ../../reference_manual/preferences/performance_settings.rst:90
msgid ""
"Playing back a video at 25 FPS means that the computer has to precalculate "
"25 images per second of video. Now, video playing software is able to do "
"this because it really focuses on this one single task. However, Krita as a "
"painting program also allows you to edit the pictures. Because Krita needs "
"to be able to do this, and a dedicated video player doesn't, Krita cannot do "
"the same kind of optimizations as a dedicated video player can."
msgstr ""
"Відтворення відео з частотою кадрів 25 кадрів за секунду потребує від "
"комп'ютера попереднього створення 25 кадрів відео за секунду. Програми для "
"відтворення відео можуть створювати ці кадри, оскільки акцент у них зроблено "
"на виконання цього єдиного завдання. Втім, Krita є програмою для малювання і "
"вона надає вам змогу редагувати зображення. Оскільки для редагування "
"зображень Krita потрібні ресурси, які ніколи не використовуватиме "
"спеціалізована програма для відтворення відео, Krita не може виконувати усі "
"оптимізації, які може виконувати спеціалізована програма для відтворення "
"відео."

#: ../../reference_manual/preferences/performance_settings.rst:92
msgid ""
"Still, an animator does need to be able to see what kind of animation they "
"are making. To do this properly, we need to decide how Krita will regenerate "
"the cache after the animator makes a change. There's fortunately a lot of "
"different options how we can do this. However, the best solution really "
"depends on what kind of computer you have and what kind of animation you are "
"making. Therefore in this tab you can customize the way how and when the "
"cache is generated."
msgstr ""
"Що б там не було, а художнику повинен мати можливість бачити, яку анімацію "
"ним створено. Щоб допомогти Krita виконати завдання належним чином, вам слід "
"вирішити, як програма оновлюватиме кеш даних після внесення художником-"
"мультиплікатором змін. На щастя, у вашому розпорядженні багато різних "
"варіантів. Втім, оптимальний варіант залежить від типу комп'ютера та типу "
"анімації. Тому, за допомогою цієї вкладки, ви можете налаштувати спосіб та "
"час створення кешу."

#: ../../reference_manual/preferences/performance_settings.rst:95
msgid "Cache Storage Backend"
msgstr "Модуль зберігання кешу"

#: ../../reference_manual/preferences/performance_settings.rst:98
msgid ""
"Animation frame cache will be stored in RAM, completely without any "
"limitations. This is also the way it was handled before 4.1. This is only "
"recommended for computers with a huge amount of RAM and animations that must "
"show full-canvas full resolution 6k at 25 fps. If you do not have a huge "
"amount (say, 64GiB) of RAM, do *not* use this option (and scale down your "
"projects)."
msgstr ""
"Кеш кадрів анімації зберігатиметься у RAM, абсолютно без обмежень. Це також "
"спосіб, у який програма працювала до версії 4.1. Це єдиний рекомендований "
"варіант для комп'ютерів із великим обсягом RAM та анімацій, які має бути "
"показано на усе полотно із роздільністю 6000 за горизонталлю та частотою "
"кадрів у 25 кадрів за секунду. Якщо на вашому комп'ютері немає великого "
"обсягу оперативної пам'яті (скажімо, 64 ГіБ), *не* використовуйте цей пункт "
"(і зменшіть масштаб вашого проєкту)."

#: ../../reference_manual/preferences/performance_settings.rst:102
msgid ""
"Please make sure your computer has enough RAM *above* the amount you "
"requested in the :guilabel:`General` tab. Otherwise you might face system "
"freezes."
msgstr ""
"Переконайтеся, що на вашому комп'ютері достатньо оперативної пам'яті, "
"*більше* за обсяг, який було визначено на вкладці :guilabel:`Загальне`. Якщо "
"пам'яті буде недостатньо, система може «замерзнути»."

#: ../../reference_manual/preferences/performance_settings.rst:104
msgid "For 1 second of FullHD @ 25 FPS you will need 200 extra MiB of Memory"
msgstr ""
"Для однієї секунди анімації у форматі FullHD з 25 кадрами за секунду вам "
"потрібно додатково 200 МіБ пам'яті."

#: ../../reference_manual/preferences/performance_settings.rst:105
msgid "In-memory"
msgstr "У пам'яті"

#: ../../reference_manual/preferences/performance_settings.rst:105
msgid ""
"For 1 second of 4K UltraHD@ 25 FPS, you will need 800 extra MiB of Memory."
msgstr ""
"Для однієї секунди анімації у форматі 4K UltraHD з 25 кадрами за секунду вам "
"потрібно додатково 800 МіБ пам'яті."

#: ../../reference_manual/preferences/performance_settings.rst:108
msgid ""
"Animation frames are stored in the hard disk in the same folder as the swap "
"file. The cache is stored in a compressed way. A little amount of extra RAM "
"is needed."
msgstr ""
"Кадри анімації зберігаються на жорсткому диску у тій самій теці, що і файл "
"резервної пам'яті на диску. Кеш зберігається у стисненому форматі. Для нього "
"потрібно трохи додаткової пам'яті."

#: ../../reference_manual/preferences/performance_settings.rst:110
msgid "On-disk"
msgstr "На диску"

#: ../../reference_manual/preferences/performance_settings.rst:110
msgid ""
"Since data transfer speed of the hard drive is slow, you might want to limit "
"the :guilabel:`Cached Frame Size` to be able to play your video at 25 fps. A "
"limit of 2500 px is usually a good choice."
msgstr ""
"Оскільки швидкість обміну даними з диском є доволі малою, вам варто "
"обмежити :guilabel:`Розмір кешованого кадру`, щоб мати змогу відтворювати "
"відео із частотою кадрів 25 кадрів за секунду. Зазвичай, 2500 пікселів є "
"добрим вибором."

#: ../../reference_manual/preferences/performance_settings.rst:113
msgid "Cache Generation Options"
msgstr "Параметри створення кешу"

#: ../../reference_manual/preferences/performance_settings.rst:115
msgid "Limit Cached Frame Size"
msgstr "Обмежити розмір кешу кадрів"

#: ../../reference_manual/preferences/performance_settings.rst:116
msgid ""
"Render scaled down version of the frame if the image is bigger than the "
"provided limit. Make sure you enable this option when using On-Disk storage "
"backend, because On-Disk storage is a little slow. Without the limit, "
"there's a good chance that it will not be able to render at full speed. "
"Lower the size to play back faster at the cost of lower resolution."
msgstr ""
"Обробляти для показу зменшену версію кадру, якщо зображення перевищує "
"встановлене обмеження. Вам слід позначити цей пункт, якщо ви використовуєте "
"модуль сховища даних на диску, оскільки сховище даних на диску є дещо "
"повільним. Без цього обмеження доволі ймовірною є неможливість обробки даних "
"на повній швидкості. Зменшіть це значення, щоб прискорити відтворення за "
"рахунок зниження роздільної здатності зображення."

#: ../../reference_manual/preferences/performance_settings.rst:117
msgid "Use Region Of Interest"
msgstr "Використовувати область зацікавлення"

#: ../../reference_manual/preferences/performance_settings.rst:118
msgid ""
"We technically only need to use the section of the image that is in view. "
"Region of interest represents that section. When the image is above the "
"configurable limit, render only the currently visible part of it."
msgstr ""
"З технічної точки зору нам потрібна лише частина зображення, яку показано на "
"екрані. Це ділянка, яка нас цікавить. Якщо параметри зображення перевищують "
"вказані, програма обробляє лише поточну видиму частину зображення."

#: ../../reference_manual/preferences/performance_settings.rst:120
msgid ""
"This allows you to set whether the animation is cached for playback in the "
"background (that is, when you're not using the computer). Then, when "
"animation is cached when pressing play, this caching will take less long. "
"However, turning off this automatic caching can save power by having your "
"computer work less."
msgstr ""
"За допомогою цього пункту можна визначити, чи кешуватиме програма дані "
"анімації для відтворення у фоновому режимі (тобто у моменти, коли ви не "
"наказуєте комп'ютеру виконати якесь інше завдання). Якщо такий кеш буде "
"створено, коли ви натиснете кнопку відтворення, кешування відбуватиметься "
"дещо швидше. Втім, вимикання автоматичного кешування може заощадити "
"живлення, оскільки комп'ютеру доведеться виконати менше роботи."
