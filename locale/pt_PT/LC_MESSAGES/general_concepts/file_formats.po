# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 11:46+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: ICC Photoshop xcf RAW Krita jpg gif webp exr psd kra\n"
"X-POFile-SpellExtra: ref EXR WebP KRA XCF JPG\n"

#: ../../general_concepts/file_formats.rst:1
msgid "The file formats category."
msgstr "A categoria de formatos de ficheiros."

#: ../../general_concepts/file_formats.rst:14
msgid "File Formats"
msgstr "Formatos de Ficheiros"

#: ../../general_concepts/file_formats.rst:16
msgid ""
"This category is for graphics file-formats. While most file-formats can be "
"looked up on wikipedia, this doesn't always explain what the format can be "
"used for and what its strengths and weaknesses are."
msgstr ""
"Esta categoria é para os formatos de ficheiros gráficos. Embora possa "
"consultar a maioria dos formatos de ficheiros na Wikipédia, nem sempre "
"explicam para que é que o formato poderá ser usado e quais as suas vantagens "
"e desvantagens."

#: ../../general_concepts/file_formats.rst:18
msgid ""
"In this category we try to describe these in a manner that can be read by "
"beginners."
msgstr ""
"Nesta categoria, iremos tentar descrevê-los de uma forma que possa ser lida "
"por principiantes."

#: ../../general_concepts/file_formats.rst:20
msgid ""
"Generally, there are the following features that people pay attention to in "
"regards to file formats:"
msgstr ""
"De uma forma geral, as seguintes características são algumas das que as "
"pessoas prestam atenção no que diz respeito aos formatos de ficheiros:"

#: ../../general_concepts/file_formats.rst:23
msgid "Compression"
msgstr "Compressão"

#: ../../general_concepts/file_formats.rst:25
msgid ""
"Compression is how the file-format tries to describe the image with as "
"little data as possible, so that the resulting file is as small as it can "
"get without losing quality."
msgstr ""
"A compressão é a forma como o formato do ficheiro tenta descrever a imagem "
"com o mínimo de dados possível, para que o ficheiro resultante seja tão "
"pequeno quanto possível sem perder qualidade."

#: ../../general_concepts/file_formats.rst:27
msgid ""
"What we generally see is that formats that are small on disk either lose "
"image quality, or require the computer to spend a lot of time thinking about "
"how the image should look."
msgstr ""
"O que se vê geralmente é que os formatos que são pequenos no disco perdem "
"qualidade da imagem ou obrigam o computador a gastar bastante tempo a pensar "
"em como ficaria a imagem."

#: ../../general_concepts/file_formats.rst:29
msgid ""
"Vector file-formats like ``SVG`` are a typical example of the latter. They "
"are really small because the technology used to create them is based on "
"mathematics, so it only stores maths-variables and can achieve very high "
"quality. The downside is that the computer needs to spend a lot of time "
"thinking about how it should look, and sometimes different programs have "
"different ways of interpreting the values. Furthermore, vector file-formats "
"imply vector graphics, which is a very different way of working than Krita "
"is specialized in."
msgstr ""
"Os formatos de ficheiros vectoriais, como o ``SVG``, são um exemplo típico "
"do último caso. São realmente pequenos, porque a tecnologia usada para os "
"criar baseia-se na matemática, pelo que só guarda variáveis algébricas, "
"conseguindo obter uma qualidade muito elevada. A desvantagem é que o "
"computador precisa de gastar bastante tempo a pensar em como ficarão as "
"coisas e, em alguns casos, os diferentes programas têm diferentes formas de "
"interpretar os valores. Para além disso, os formatos de ficheiros vectoriais "
"implicam gráficos vectoriais, que são uma forma de trabalhar muito diferente "
"da forma em que se especializa o Krita."

#: ../../general_concepts/file_formats.rst:31
msgid ""
":ref:`Lossy file formats <lossy_compression>`, like ``JPG`` or ``WebP`` are "
"an example of small on disk, but lowering the quality, and are best used for "
"very particular types of images. Lossy thus means that the file format plays "
"fast and loose with describing your image to reduce filesize."
msgstr ""
":ref:`Os formatos de ficheiros com perdas <lossy_compression>`, como o "
"``JPG`` ou o ``WebP`` são um exemplo de pouco espaço em disco, mas com uma "
"quebra de qualidade acentuada, sendo mais adequados para determinados tipos "
"de imagens em particular. O formato com perdas significa assim que o formato "
"do ficheiro é rápido e pouco detalhado a descrever a sua imagem, com o "
"objectivo de reduzir o tamanho dos ficheiros."

#: ../../general_concepts/file_formats.rst:33
msgid ""
":ref:`Non-lossy or lossless formats <lossless_compression>`, like ``PNG``, "
"``GIF`` or ``BMP`` are in contrast, much heavier on disk, but much more "
"likely to retain quality."
msgstr ""
"Os :ref:`formatos com ou sem perdas <lossless_compression>`, como o ``PNG``, "
"o ``GIF`` ou o ``BMP`` estão em contraste, são muito mais pesados a nível de "
"disco, mas com muito mais hipóteses de manter a qualidade."

#: ../../general_concepts/file_formats.rst:35
msgid ""
"Then, there's proper working file formats like Krita's ``KRA``, Gimp's "
"``XCF``, Photoshop's ``PSD``, but also interchange formats like ``ORA`` and "
"``EXR``. These are the heaviest on the hard-drive and often require special "
"programs to open them up, but on the other hand these are meant to keep your "
"working environment intact, and keep all the layers and guides in them."
msgstr ""
"Depois, existem formatos de trabalho relativamente funcionais, como o "
"``KRA`` do Krita, o ``XCF`` do Gimp ou o ``PSD`` do Photoshop, mas também "
"formatos interoperáveis, como o ``ORA`` ou o ``EXR``. Estes são os mais "
"pesados a nível do disco e normalmente obrigam ao uso de programas especiais "
"para os abrir; contudo, estes pretendem ser usados para manter o seu "
"ambiente de trabalho intacto, mantendo todas as camadas e guias neles."

#: ../../general_concepts/file_formats.rst:38
msgid "Metadata"
msgstr "Meta-dados"

#: ../../general_concepts/file_formats.rst:40
msgid ""
"Metadata is the ability of a file format to contain information outside of "
"the actual image contents. This can be human readable data, like the date of "
"creation, the name of the author, a description of the image, but also "
"computer readable data, like an icc-profile which tells the computer about "
"the qualities of how the colors inside the file should be read."
msgstr ""
"Os meta-dados são a capacidade de um formato de ficheiro conseguir ter "
"informações fora do conteúdo actual da imagem. Estes poderão ser dados "
"legíveis para o utilizador, como a data de criação, o nome do autor, uma "
"descrição da imagem, mas também dados legíveis pelo computador, como o "
"perfil ICC que diz ao computador as características com que as cores dentro "
"do ficheiro devem ser lidas."

#: ../../general_concepts/file_formats.rst:43
msgid "Openness"
msgstr "Abertura"

#: ../../general_concepts/file_formats.rst:45
msgid ""
"This is a bit of an odd quality, but it's about how easy it to open or "
"recover the file, and how widely it's supported."
msgstr ""
"Esta é uma qualidade um pouco estranha, mas está relacionada com a "
"facilidade de abrir ou recuperar o ficheiro e quão abrangente é o seu "
"suporte."

#: ../../general_concepts/file_formats.rst:47
msgid ""
"Most internal file formats, like PSD are completely closed, and it's really "
"difficult for human outsiders to recover the data inside without opening "
"Photoshop. Other examples are camera raw files which have different "
"properties per camera manufacturer."
msgstr ""
"A maioria dos formatos de ficheiros internos, como o PSD, são completamente "
"fechados e é realmente difícil para os utilizadores externos recuperarem os "
"dados sem abrir o Photoshop. Os outros exemplos são os ficheiros RAW das "
"câmaras que têm propriedades diferentes por fabricante da câmara."

#: ../../general_concepts/file_formats.rst:49
msgid ""
"SVG, as a vector file format, is on the other end of the spectrum, and can "
"be opened with any text-editor and edited."
msgstr ""
"O SVG, como um formato de ficheiros vectorial, está no outro extremo do "
"espectro e pode ser aberto e editado com qualquer editor de texto."

#: ../../general_concepts/file_formats.rst:51
msgid ""
"Most formats are in-between, and thus there's also a matter of how widely "
"supported the format is. JPG and PNG cannot be read or edited by human eyes, "
"but the vast majority of programs can open them, meaning the owner has easy "
"access to them."
msgstr ""
"A maioria dos formatos situa-se a um nível intermédio e, como tal, é tudo "
"uma questão de quão amplamente suportado é o formato. Os formatos JPG e PNG "
"não podem ser lidos ou editados por humanos, mas a grande maioria dos "
"programas conseguem abri-los, o que significa que o dono tem acesso simples "
"a eles."

#: ../../general_concepts/file_formats.rst:53
msgid "Contents:"
msgstr "Conteúdo:"
