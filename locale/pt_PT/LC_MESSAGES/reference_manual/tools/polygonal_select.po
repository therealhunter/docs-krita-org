# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-06-17 15:03+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: icons Krita image Kritamouseleft selectionsbasics kbd\n"
"X-POFile-SpellExtra: generalsettings polygonalselecttool images alt ref\n"
"X-POFile-SpellExtra: toolselectpolygon mouseleft mouseright\n"
"X-POFile-SpellExtra: Kritamouseright\n"

#: ../../<generated>:1
msgid "Anti-aliasing"
msgstr "Suavização"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: botão esquerdo"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: botão direito"

#: ../../<rst_epilog>:70
msgid ""
".. image:: images/icons/polygonal_select_tool.svg\n"
"   :alt: toolselectpolygon"
msgstr ""
".. image:: images/icons/polygonal_select_tool.svg\n"
"   :alt: ferramenta de selecção poligonal"

#: ../../reference_manual/tools/polygonal_select.rst:1
msgid "Krita's polygonal selection tool reference."
msgstr "A referência da ferramenta de selecção poligonal do Krita."

#: ../../reference_manual/tools/polygonal_select.rst:11
msgid "Tools"
msgstr "Ferramentas"

#: ../../reference_manual/tools/polygonal_select.rst:11
msgid "Polygon"
msgstr "Polígono"

#: ../../reference_manual/tools/polygonal_select.rst:11
msgid "Selection"
msgstr "Selecção"

#: ../../reference_manual/tools/polygonal_select.rst:11
msgid "Polygonal Selection"
msgstr "Selecção Poligonal"

#: ../../reference_manual/tools/polygonal_select.rst:16
msgid "Polygonal Selection Tool"
msgstr "Ferramenta de Selecção Poligonal"

#: ../../reference_manual/tools/polygonal_select.rst:18
msgid "|toolselectpolygon|"
msgstr "|toolselectpolygon|"

#: ../../reference_manual/tools/polygonal_select.rst:20
msgid ""
"This tool, represented by a polygon with a dashed border, allows you to "
"make :ref:`selections_basics` of a polygonal area point by point. Click "
"where you want each point of the Polygon to be. Double click to end your "
"polygon and finalize your selection area."
msgstr ""
"Esta ferramenta, representada com um polígono com um contorno tracejado, "
"permite-lhe efectuar :ref:`selections_basics` de uma área poligonal ponto-a-"
"ponto. Carregue onde deseja que fique cada ponto do Polígono. Faça duplo-"
"click para terminar o seu polígono e finalizar a sua área de selecção."

#: ../../reference_manual/tools/polygonal_select.rst:24
msgid "Hotkeys and Sticky keys"
msgstr "Atalhos e Teclas Fixas"

#: ../../reference_manual/tools/polygonal_select.rst:26
msgid ""
":kbd:`R` sets the selection to 'replace' in the tool options, this is the "
"default mode."
msgstr ""
":kbd:`R` configura a selecção para `substituição` nas opções da ferramenta; "
"este é o modo por omissão."

#: ../../reference_manual/tools/polygonal_select.rst:27
msgid ":kbd:`A` sets the selection to 'add' in the tool options."
msgstr ":kbd:`A` configura a selecção como `adição` nas opções da ferramenta."

#: ../../reference_manual/tools/polygonal_select.rst:28
msgid ":kbd:`S` sets the selection to 'subtract' in the tool options."
msgstr ""
":kbd:`S` configura a selecção para `subtracção` nas opções da ferramenta."

#: ../../reference_manual/tools/polygonal_select.rst:29
msgid ""
":kbd:`Shift +` |mouseleft| sets the subsequent selection to 'add'. You can "
"release the :kbd:`Shift` key while dragging, but it will still be set to "
"'add'. Same for the others."
msgstr ""
":kbd:`Shift` + |mouseleft| configura a selecção subsequente para `adição`. "
"Poderá largar a tecla :kbd:`Shift` enquanto arrasta, mas continuará à mesma "
"no modo de 'adição'. O mesmo se aplica aos outros."

#: ../../reference_manual/tools/polygonal_select.rst:30
msgid ":kbd:`Alt +` |mouseleft| sets the subsequent selection to 'subtract'."
msgstr ""
":kbd:`Alt` + |mouseleft| configura a selecção subsequente como `subtracção`."

#: ../../reference_manual/tools/polygonal_select.rst:31
msgid ":kbd:`Ctrl +` |mouseleft| sets the subsequent selection to 'replace'."
msgstr ""
":kbd:`Ctrl` + |mouseleft| configura a selecção subsequente como "
"`substituição`."

#: ../../reference_manual/tools/polygonal_select.rst:32
msgid ""
":kbd:`Shift + Alt +` |mouseleft| sets the subsequent selection to "
"'intersect'."
msgstr ""
":kbd:`Shift + Alt` + |mouseleft| configura a selecção subsequente como "
"`intersecção`."

#: ../../reference_manual/tools/polygonal_select.rst:36
msgid "Hovering over a selection allows you to move it."
msgstr "Se passar o cursor sobre uma selecção permitir-lhe-á movê-la."

#: ../../reference_manual/tools/polygonal_select.rst:37
msgid ""
"|mouseright| will open up a selection quick menu with amongst others the "
"ability to edit the selection."
msgstr ""
"Se usar o |mouseright|, irá abrir um menu de selecção rápida que, entre "
"outras coisas, terá a possibilitar de editar a selecção."

#: ../../reference_manual/tools/polygonal_select.rst:41
msgid ""
"You can switch the behavior of the :kbd:`Alt` key to use the :kbd:`Ctrl` key "
"instead by toggling the switch in the :ref:`general_settings`."
msgstr ""
"Poderá mudar o comportamento da tecla :kbd:`Alt` para usar como alternativa "
"o :kbd:`Ctrl`, comutando o interruptor na :ref:`general_settings`."

#: ../../reference_manual/tools/polygonal_select.rst:44
msgid "Tool Options"
msgstr "Opções da Ferramenta"

#: ../../reference_manual/tools/polygonal_select.rst:47
msgid ""
"This toggles whether or not to give selections feathered edges. Some people "
"prefer hard-jagged edges for their selections."
msgstr ""
"Isto indica se são usados contornos leves nas selecções. Algumas pessoas "
"preferem arestas vincadas para as suas selecções."
