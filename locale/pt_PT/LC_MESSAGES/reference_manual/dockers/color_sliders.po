# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-17 16:43+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: en Color image menuselection slider images docker\n"
"X-POFile-SpellExtra: Krita dockers Luma\n"

#: ../../reference_manual/dockers/color_sliders.rst:1
msgid "Overview of the color sliders docker."
msgstr "Introdução à área de barras de cores."

#: ../../reference_manual/dockers/color_sliders.rst:11
#: ../../reference_manual/dockers/color_sliders.rst:16
msgid "Color Sliders"
msgstr "Barras de Cores"

#: ../../reference_manual/dockers/color_sliders.rst:11
msgid "Color"
msgstr "Cor"

#: ../../reference_manual/dockers/color_sliders.rst:11
msgid "Color Selector"
msgstr "Selecção de Cores"

#: ../../reference_manual/dockers/color_sliders.rst:11
msgid "Hue"
msgstr "Tom"

#: ../../reference_manual/dockers/color_sliders.rst:11
msgid "Saturation"
msgstr "Saturação"

#: ../../reference_manual/dockers/color_sliders.rst:11
msgid "Value"
msgstr "Valor"

#: ../../reference_manual/dockers/color_sliders.rst:11
msgid "Brightness"
msgstr "Brilho"

#: ../../reference_manual/dockers/color_sliders.rst:11
msgid "Lightness"
msgstr "Iluminação"

#: ../../reference_manual/dockers/color_sliders.rst:11
msgid "Intensity"
msgstr "Intensidade"

#: ../../reference_manual/dockers/color_sliders.rst:11
msgid "Luma"
msgstr "Luma"

#: ../../reference_manual/dockers/color_sliders.rst:11
msgid "Luminosity"
msgstr "Luminosidade"

#: ../../reference_manual/dockers/color_sliders.rst:20
msgid ""
"This docker has been removed in 4.1. It will return in some form in the "
"future."
msgstr "Esta área foi removida no 4.1. Irá voltar de alguma forma no futuro."

#: ../../reference_manual/dockers/color_sliders.rst:22
msgid "A small docker with Hue, Saturation and Lightness bars."
msgstr ""
"Uma pequena área acoplável com barras de Tom (Matiz), Saturação e "
"Luminosidade."

#: ../../reference_manual/dockers/color_sliders.rst:25
msgid ".. image:: images/dockers/Color-slider-docker.png"
msgstr ".. image:: images/dockers/Color-slider-docker.png"

#: ../../reference_manual/dockers/color_sliders.rst:26
msgid ""
"You can configure this docker via :menuselection:`Settings --> Configure "
"Krita --> Color Selector Settings --> Color Sliders`."
msgstr ""
"Poderá configurar esta área acoplável com a opção :menuselection:"
"`Configuração --> Configurar o Krita --> Configuração do Selector de Cores --"
"> Barras de Cores`."

#: ../../reference_manual/dockers/color_sliders.rst:28
msgid ""
"There, you can select which sliders you would like to see added, allowing "
"you to even choose multiple lightness sliders together."
msgstr ""
"Aí, poderá seleccionar quais as barras que deseja adicionar, permitindo-lhe "
"até escolher várias barras de luminosidade em conjunto."
