# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-17 18:05+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: en icons Krita image pô Kritamouseright\n"
"X-POFile-SpellExtra: brushenginelocking images alt mouseright guilabel\n"
"X-POFile-SpellExtra: brushes\n"

#: ../../<generated>:1
msgid "Unlock (Keep Locked)"
msgstr "Desbloquear (Manter Bloqueado)"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: botão direito"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:1
msgid "How to keep brush settings locked in Krita."
msgstr "Como manter as configurações dos pincéis bloqueadas no Krita."

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:11
#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:16
msgid "Locked Brush Settings"
msgstr "Configurações dos Pincéis Bloqueadas"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:19
msgid ""
"Normally, a changing to a different brush preset will change all brush "
"settings. Locked presets are a way for you to prevent Krita from changing "
"all settings. So, if you want to have the texture be that same over all "
"brushes, you lock the texture parameter. That way, all brush-preset you "
"select will now share the same texture!"
msgstr ""
"Normalmente, uma alteração a uma predefinição do pincel diferente irá "
"modificar todas as definições do pincel. As predefinições bloqueadas são uma "
"forma de você evitar que o Krita mude todas as definições. Como tal, se "
"quiser que a textura seja a mesma em todos os pincéis, poderá bloquear o "
"parâmetro da textura. Desta forma, todas as predefinições de pincéis que "
"seleccionar irão partilhar a mesma textura!"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:22
msgid "Locking a brush parameter"
msgstr "Bloquear um parâmetro do pincel"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:25
msgid ".. image:: images/brushes/Krita_2_9_brushengine_locking_01.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_locking_01.png"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:26
msgid ""
"To lock an option, |mouseright| the little lock icon next to the parameter "
"name, and set it to :guilabel:`Lock`. It will now be highlighted to show "
"it's locked:"
msgstr ""
"Para bloquear uma opção, use o |mouseright| sobre o pequeno ícone de cadeado "
"a seguir ao nome do parâmetro e mude-o para :guilabel:`Bloquear`. O mesmo "
"ficará realçado, para mostrar que está bloqueado:"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:29
msgid ".. image:: images/brushes/Krita_2_9_brushengine_locking_02.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_locking_02.png"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:30
msgid "And on the canvas, it will show that the texture-option is locked."
msgstr ""
"E, na área de desenho, irá mostrar que a opção da textura está bloqueada."

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:33
msgid ".. image:: images/brushes/Krita_2_9_brushengine_locking_04.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_locking_04.png"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:35
msgid "Unlocking a brush parameter"
msgstr "Desbloquear um parâmetro do pincel"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:37
msgid "To *unlock*, |mouseright| the icon again."
msgstr "Para *desbloquear*, use o |mouseright| de novo sobre o ícone."

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:40
msgid ".. image:: images/brushes/Krita_2_9_brushengine_locking_03.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_locking_03.png"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:41
msgid "There will be two options:"
msgstr "Existirão duas opções:"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:43
msgid "Unlock (Drop Locked)"
msgstr "Desbloquear (Tirar o Bloqueio)"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:44
msgid ""
"This will get rid of the settings of the locked parameter and take that of "
"the active brush preset. So if your brush had no texture on, using this "
"option will revert it to having no texture."
msgstr ""
"Isto irá eliminar as configurações do parâmetro bloqueado e irá usar as da "
"predefinição do pincel activa. Como tal, se o seu pincel não tinha nenhuma "
"textura activa, o uso desta opção voltará a a pô-la sem qualquer textura."

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:46
msgid "This will keep the settings of the parameter even though it's unlocked."
msgstr ""
"Isto irá manter as definições do parâmetro, mesmo que esteja desbloqueado."
