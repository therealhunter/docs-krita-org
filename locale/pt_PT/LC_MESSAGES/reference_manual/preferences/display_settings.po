# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-06 03:34+0200\n"
"PO-Revision-Date: 2019-05-20 00:21+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Tri en ANGLE Krita image Direct AMD images Bi\n"
"X-POFile-SpellExtra: KritaPreferencesDisplay Radeon preferences HDR\n"

#: ../../<generated>:1
msgid "Hide Layer thumbnail popup"
msgstr "Esconder a área da miniatura da camada"

#: ../../reference_manual/preferences/display_settings.rst:1
msgid "Display settings in Krita."
msgstr "Configuração da visualização no Krita."

#: ../../reference_manual/preferences/display_settings.rst:12
#: ../../reference_manual/preferences/display_settings.rst:24
msgid "OpenGL"
msgstr "OpenGL"

#: ../../reference_manual/preferences/display_settings.rst:12
#: ../../reference_manual/preferences/display_settings.rst:84
msgid "Canvas Border"
msgstr "Contorno da Área de Desenho"

#: ../../reference_manual/preferences/display_settings.rst:12
msgid "Preferences"
msgstr "Preferências"

#: ../../reference_manual/preferences/display_settings.rst:12
msgid "Settings"
msgstr "Configuração"

#: ../../reference_manual/preferences/display_settings.rst:12
msgid "Transparency Checkers"
msgstr "Xadrez da Transparência"

#: ../../reference_manual/preferences/display_settings.rst:12
msgid "Canvas Graphics Acceleration"
msgstr "Aceleração Gráfica da Área de Desenho"

#: ../../reference_manual/preferences/display_settings.rst:12
msgid "Display"
msgstr "Visualização"

#: ../../reference_manual/preferences/display_settings.rst:17
msgid "Display Settings"
msgstr "Configuração da Aparência"

#: ../../reference_manual/preferences/display_settings.rst:20
msgid ".. image:: images/preferences/Krita_Preferences_Display.png"
msgstr ".. image:: images/preferences/Krita_Preferences_Display.png"

#: ../../reference_manual/preferences/display_settings.rst:21
msgid "Here various settings for the rendering of Krita can be edited."
msgstr "Aqui poderá editar várias definições para o desenho no Krita."

#: ../../reference_manual/preferences/display_settings.rst:26
msgid ""
"**For Krita 3.3 or later: Reworded as \"*Canvas Graphics Acceleration*\"**"
msgstr ""
"**Para o Krita 3.3 ou posterior: Mudança para \"*Aceleração Gráfica da Área "
"de Desenho*\"**"

#: ../../reference_manual/preferences/display_settings.rst:28
msgid ""
"OpenGL is a bit of code especially for graphics cards. Graphics cards a "
"dedicate piece of hardware for helping your computer out with graphics "
"calculations, which Krita uses a lot. All modern computer have graphics "
"cards."
msgstr ""
"O OpenGL é uma biblioteca de código especial para as placas gráficas. As "
"placas gráficas são uma placa de 'hardware' dedicada a ajudar o seu "
"computador com os cálculos gráficos, os quais o Krita usa bastante. Todos os "
"computadores modernos têm placas gráficas."

#: ../../reference_manual/preferences/display_settings.rst:30
msgid ""
"**For Krita 3.3 or later:** On Windows, Krita also supports using Direct3D "
"instead with the help of the ANGLE library. ANGLE works by converting the "
"OpenGL functions that Krita makes use of to the equivalent in Direct3D. It "
"may (or may not) be slower than native OpenGL, but it has better "
"compatibility with typical Windows graphics drivers."
msgstr ""
"**Para o Krita 3.3 ou posterior:** No Windows, o Krita também suporta o uso "
"do Direct3D em alternativa, com a ajuda da biblioteca ANGLE. O ANGLE "
"funciona através da conversão das funções de OpenGL que o Krita usa, "
"convertendo-as para as suas equivalentes no Direct3D. Poderá (ou não) ser "
"mais lenta que o OpenGL nativo, mas tem uma melhor compatibilidade com os "
"controladores gráficos típicos do Windows."

#: ../../reference_manual/preferences/display_settings.rst:32
msgid ""
"Enable OpenGL **(For Krita 3.3 or later: Reworded as *Canvas Graphics "
"Acceleration*)**"
msgstr ""
"Activar o OpenGL **(Para o Krita 3.3 ou posterior: Mudança para "
"\"*Aceleração Gráfica da Área de Desenho*\")**"

#: ../../reference_manual/preferences/display_settings.rst:33
msgid ""
"Selecting this checkbox will enable the OpenGL / ANGLE canvas drawing mode. "
"With a decent graphics card this should give faster feedback on brushes and "
"tools. Also the canvas operations like Rotate, Zoom and Pan should be "
"considerably faster."
msgstr ""
"A marcação desta opção irá activar o modo de desenho com o OpenGL / ANGLE. "
"Com uma placa gráfica decente, isto deverá gerar uma reacção mais rápida com "
"os pincéis e ferramentas. Da mesma forma, as operações na área de desenho, "
"como a Rotação, a Ampliação e o Posicionamento deverão ser bastante mais "
"rápidas."

#: ../../reference_manual/preferences/display_settings.rst:35
msgid "For Krita 3.3 or later:"
msgstr "Para o Krita 3.3 ou posterior:"

#: ../../reference_manual/preferences/display_settings.rst:35
msgid "Renderer"
msgstr "Motor de representação"

#: ../../reference_manual/preferences/display_settings.rst:36
msgid ""
"*On Windows:* You can switch between native OpenGL or ANGLE Direct3D 11 "
"rendering. The usual recommendation is to leave it as \"Auto\", which Krita "
"will decide the best to use based on some internal compatibility checking. "
"Changes to this option require a restart of Krita to take effect."
msgstr ""
"*No Windows:* Poderá comutar entre o desenho com o OpenGL nativo ou o ANGLE "
"Direct3D 11. A recomendação normal é que deixe como \"Auto\", onde o Krita "
"irá decidir o melhor método a usar, com base nalgumas validações de "
"compatibilidade internas. As alterações a esta opção obrigam a que reinicie "
"o Krita."

#: ../../reference_manual/preferences/display_settings.rst:37
msgid "Use Texture Buffer"
msgstr "Usar o 'Buffer' de Texturas"

#: ../../reference_manual/preferences/display_settings.rst:38
msgid ""
"This setting utilizes the graphics card's buffering capabilities to speed "
"things up a bit. Although for now, this feature may be broken on some AMD/"
"Radeon cards and may work fine on some Intel graphics cards."
msgstr ""
"Esta opção usa as capacidades de desenho da placa gráfica para acelerar um "
"pouco as coisas. Por agora, esta funcionalidade poderá estar danificada em "
"algumas placas AMD/Radeon e poderá funcionar perfeitamente em algumas placas "
"gráficas da Intel."

#: ../../reference_manual/preferences/display_settings.rst:40
msgid ""
"The user can choose which scaling mode to use while zooming the canvas. The "
"choice here only affects the way the image is displayed during canvas "
"operations and has no effect on how Krita scales an image when a "
"transformation is applied."
msgstr ""
"O utilizador poderá escolher o modo de escala a usar quando ampliar a área "
"de desenho. A escolha aqui só afecta a forma como é apresentada a imagem "
"durante as operações com a área de desenho, não fazendo efeito sobre a forma "
"como o Krita ajusta a escala de uma imagem quando aplicar uma transformação."

#: ../../reference_manual/preferences/display_settings.rst:42
msgid "Nearest Neighbour"
msgstr "Vizinho Mais Próximo"

#: ../../reference_manual/preferences/display_settings.rst:43
msgid ""
"This is the fastest and crudest filtering method. While fast, this results "
"in a large number of artifacts - 'blockiness' during magnification, and "
"aliasing and shimmering during minification."
msgstr ""
"Este é o método de filtragem mais rápido e em bruto. Embora seja rápido, "
"poderá gerar um grande número de artefactos - geração de 'blocos' durante a "
"ampliação e alguma perda de suavização e nitidez na redução."

#: ../../reference_manual/preferences/display_settings.rst:44
msgid "Bilinear Filtering"
msgstr "Filtragem Bi-Linear"

#: ../../reference_manual/preferences/display_settings.rst:45
msgid ""
"This is the next step up. This removes the 'blockiness' seen during "
"magnification and gives a smooth looking result. For most purposes this "
"should be a good trade-off between speed and quality."
msgstr ""
"Este é o próximo passo. Isto remove o 'aspecto em blocos' que aparece na "
"ampliação e gera um resultado com aspecto mais suavizado. Para a maioria dos "
"casos, isto deverá ser um bom compromisso entre a velocidade e a qualidade."

#: ../../reference_manual/preferences/display_settings.rst:46
msgid "Trilinear Filtering"
msgstr "Filtragem Tri-Linear"

#: ../../reference_manual/preferences/display_settings.rst:47
msgid "This should give a little better result than Bilinear Filtering."
msgstr ""
"Esta deverá gerar um resultado ligeiramente melhor que a Filtragem Bi-Linear."

#: ../../reference_manual/preferences/display_settings.rst:49
msgid "Scaling Mode"
msgstr "Modo de Escala"

#: ../../reference_manual/preferences/display_settings.rst:49
msgid "High Quality Filtering"
msgstr "Filtragem de Alta Qualidade"

#: ../../reference_manual/preferences/display_settings.rst:49
msgid ""
"Only available when your graphics card supports OpenGL 3.0. As the name "
"suggests, this setting provides the best looking image during canvas "
"operations."
msgstr ""
"Só está disponível quando a sua placa gráfica suportar o OpenGL 3.0. Como "
"diz o nome, esta opção oferece a imagem com melhor aparência durante as "
"operações com a área de desenho."

#: ../../reference_manual/preferences/display_settings.rst:54
msgid "HDR"
msgstr "HDR"

#: ../../reference_manual/preferences/display_settings.rst:58
msgid "These settings are only available when using Windows."
msgstr "Estas opções só estão disponíveis se estiver a usar o Windows."

#: ../../reference_manual/preferences/display_settings.rst:60
msgid ""
"Since 4.2 Krita can not just edit floating point images, but also render "
"them on screen in a way that an HDR capable setup can show them as HDR "
"images."
msgstr ""
"Desde o 4.2, o Krita não só consegue editar imagens de vírgula flutuante, "
"como também desenhá-las no ecrã de forma que uma configuração que suporte o "
"HDR as consiga mostrar como imagens HDR."

#: ../../reference_manual/preferences/display_settings.rst:62
msgid ""
"The HDR settings will show you the display format that Krita can handle, and "
"the current output format. You will want to set the preferred output format "
"to the one closest to what your display can handle to make full use of it."
msgstr ""
"A configuração do HDR mostrar-lhe-á o formato de visualização que o Krita "
"consegue usar, assim como o formato de saída actual. Irá querer definir o "
"formato de saída preferido para o que se aproxime mais do que o seu ecrã "
"consegue usar na totalidade."

#: ../../reference_manual/preferences/display_settings.rst:64
msgid "Display Format"
msgstr "Formato de Visualização"

#: ../../reference_manual/preferences/display_settings.rst:65
msgid ""
"The format your display is in by default. If this isn't higher than 8bit, "
"there's a good chance your monitor is not an HDR monitor as far as Krita can "
"tell. This can be a hardware issue, but also a graphics driver issue. Check "
"if other HDR applications, or the system HDR settings are configured "
"correctly."
msgstr ""
"O formato em que se se encontra o seu ecrã por omissão. Se este não for "
"superior a 8 bits, é bastante possível que o seu monitor não seja um monitor "
"HDR, tanto quanto o Krita consegue apurar. Isto poderá ser uma questão de "
"'hardware', mas também poderá ser uma questão do controlador gráfico. "
"Verifique se outras aplicações de HDR ou se as definições de HDR do sistema "
"estão devidamente configuradas."

#: ../../reference_manual/preferences/display_settings.rst:66
msgid "Current Output format"
msgstr "Formato de Saída Actual"

#: ../../reference_manual/preferences/display_settings.rst:67
msgid "What Krita is rendering the canvas to currently."
msgstr "Com que formato o Krita está a tentar desenhar a área de desenho."

#: ../../reference_manual/preferences/display_settings.rst:69
msgid "Preferred Output Format"
msgstr "Formato de Saída Preferido"

#: ../../reference_manual/preferences/display_settings.rst:69
msgid ""
"Which surface type you prefer. This should be ideally the closest to the "
"display format, but perhaps due to driver issues you might want to try other "
"formats. This requires a restart."
msgstr ""
"Qual o tipo de superfície que prefere. Este deverá ser idealmente o mais "
"próximo do formato de visualização, mas talvez por problemas no controlador, "
"poderá querer experimentar outros formatos. Isto obriga a reiniciar a "
"aplicação."

#: ../../reference_manual/preferences/display_settings.rst:72
msgid "Transparency Checkboxes"
msgstr "Opções de Transparência"

#: ../../reference_manual/preferences/display_settings.rst:74
msgid ""
"Krita supports layer transparency. Of course, the nasty thing is that "
"transparency can't be seen. So to indicate transparency at the lowest layer, "
"we use a checker pattern. This part allows you to configure it."
msgstr ""
"O Krita suporta a transparência das camadas. Obviamente, o efeito paradoxal "
"é que a transparência não se consegue ver. Como tal, para representar a "
"transparência na camada mais abaixo, é usado um padrão em xadrez. Esta parte "
"permite-lhe configurá-la."

#: ../../reference_manual/preferences/display_settings.rst:76
msgid "Size"
msgstr "Tamanho"

#: ../../reference_manual/preferences/display_settings.rst:77
msgid ""
"This sets the size of the checkers which show up in transparent parts of an "
"image."
msgstr ""
"Isto define o tamanho do xadrez que aparece nas partes transparentes de uma "
"imagem."

#: ../../reference_manual/preferences/display_settings.rst:78
#: ../../reference_manual/preferences/display_settings.rst:86
#: ../../reference_manual/preferences/display_settings.rst:98
msgid "Color"
msgstr "Cor"

#: ../../reference_manual/preferences/display_settings.rst:79
msgid "The user can set the colors for the checkers over here."
msgstr "O utilizador pode definir as cores do padrão em xadrez aqui."

#: ../../reference_manual/preferences/display_settings.rst:81
msgid "Move Checkers When Scrolling"
msgstr "Mover o Xadrez no Deslocamento"

#: ../../reference_manual/preferences/display_settings.rst:81
msgid ""
"When selected the checkers will move along with opaque elements of an image "
"during canvas Panning, Zooming, etc.  Otherwise the checkers remain "
"stationary and only the opaque parts of an image will move."
msgstr ""
"Quando estiver seleccionado, o xadrez mover-se-á com os elementos opacos de "
"uma imagem durante o Posicionamento, Ampliação, etc. de uma imagem. Caso "
"contrário, os padrões em xadrez ficarão estáticos e só se irão mover as "
"partes opacas da imagem."

#: ../../reference_manual/preferences/display_settings.rst:87
msgid ""
"The user can select the color for the canvas i.e. the space beyond a "
"document's boundaries."
msgstr ""
"O utilizador pode seleccionar a cor da área de desenho, i.e. o espaço para "
"lá dos limites de um documento."

#: ../../reference_manual/preferences/display_settings.rst:89
msgid "Hide Scrollbars"
msgstr "Esconder as Barras de Deslocamento"

#: ../../reference_manual/preferences/display_settings.rst:89
msgid "Selecting this will hide the scrollbars in all view modes."
msgstr ""
"Se seleccionar isto, irá esconder as barras de deslocamento em todos os "
"modos de visualização."

#: ../../reference_manual/preferences/display_settings.rst:92
msgid "Pixel Grid"
msgstr "Grelha de Pixels"

#: ../../reference_manual/preferences/display_settings.rst:96
msgid ""
"This allows configuring an automatic pixel-by-pixel grid, which is very "
"useful for doing pixel art."
msgstr ""
"Isto permite configurar uma grelha pixel-por-pixel automática, o que é muito "
"útil para gráficos com pixels."

#: ../../reference_manual/preferences/display_settings.rst:99
msgid "The color of the grid."
msgstr "A cor da grelha."

#: ../../reference_manual/preferences/display_settings.rst:101
msgid "Start Showing at"
msgstr "Começar a Mostrar em"

#: ../../reference_manual/preferences/display_settings.rst:101
msgid ""
"This determines the zoom level at which the pixel grid starts showing, as "
"showing it when the image is zoomed out a lot will make the grid overwhelm "
"the image, and is thus counter productive."
msgstr ""
"Isto define o nível de ampliação em que começa a aparecer a grelha de "
"pixels, já que, quando a imagem é bastante reduzida, fará com que a grelha "
"se sobreponha à imagem, o que se torna contraproducente."

#: ../../reference_manual/preferences/display_settings.rst:104
msgid "Miscellaneous"
msgstr "Diversos"

#: ../../reference_manual/preferences/display_settings.rst:106
msgid "Color Channels in Color"
msgstr "Canais de Cores a Cores"

#: ../../reference_manual/preferences/display_settings.rst:107
msgid ""
"This is supposed to determine what to do when only a single channel is "
"selected in the channels docker, but it doesn't seem to work."
msgstr ""
"Isto é suporto determinar o que fazer quando só está seleccionado um canal "
"na área de canais, mas não parece estar a funcionar."

#: ../../reference_manual/preferences/display_settings.rst:108
msgid "Enable Curve Anti-Aliasing"
msgstr "Activar a Suavização da Curva"

#: ../../reference_manual/preferences/display_settings.rst:109
msgid ""
"This allows anti-aliasing on previewing curves, like the ones for the circle "
"tool, or the path tool."
msgstr ""
"Isto permite a suavização na antevisão das curvas, como as da ferramenta de "
"círculos ou de caminhos."

#: ../../reference_manual/preferences/display_settings.rst:110
msgid "Enable Selection Outline Anti-Aliasing"
msgstr "Activar a Suavização do Contorno de Selecção"

#: ../../reference_manual/preferences/display_settings.rst:111
msgid ""
"This allows automatic anti-aliasing on selection. It makes the selection "
"feel less jaggy and more precise."
msgstr ""
"Isto permite uma suavização automática na selecção. Cria menos perturbações "
"na selecção, tornando-a mais precisa."

#: ../../reference_manual/preferences/display_settings.rst:112
msgid "Hide window scrollbars."
msgstr "Esconder as barras de deslocamento da janela."

#: ../../reference_manual/preferences/display_settings.rst:113
msgid "Hides the scrollbars on the canvas."
msgstr "Esconde as barras de deslocamento na área de desenho."

#: ../../reference_manual/preferences/display_settings.rst:115
msgid "This disables the thumbnail that you get when hovering over a layer."
msgstr ""
"Isto desactiva a miniatura que obtém ao passar o cursor sobre uma camada."
