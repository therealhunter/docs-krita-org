msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-08 17:38+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: en icons Krita filtercolortoalpha image\n"
"X-POFile-SpellExtra: Kritamouseright layerstylehack brush images alt ref\n"
"X-POFile-SpellExtra: tips mouseright\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: botão direito"

#: ../../tutorials/krita-brush-tips/outline.rst:None
msgid ""
".. image:: images/brush-tips/Krita-layerstyle_hack.png\n"
"   :alt: image demonstrating the layer style hack for this effect"
msgstr ""
".. image:: images/brush-tips/Krita-layerstyle_hack.png\n"
"   :alt: imagem que demonstra o truque do estilo da camada para este efeito"

#: ../../tutorials/krita-brush-tips/outline.rst:None
msgid ""
".. image:: images/brush-tips/Krita-layerstyle_hack2.png\n"
"   :alt: image demonstrating the layer style hack for this effect"
msgstr ""
".. image:: images/brush-tips/Krita-layerstyle_hack2.png\n"
"   :alt: imagem que demonstra o truque do estilo da camada para este efeito"

#: ../../tutorials/krita-brush-tips/outline.rst:1
msgid "A tutorial about painting outline while you draw with brush"
msgstr "Um tutorial sobre a pintura de contornos enquanto desenha com o pincel"

#: ../../tutorials/krita-brush-tips/outline.rst:13
msgid "Brush-tips:Outline"
msgstr "Dicas-pincéis:Contornos"

#: ../../tutorials/krita-brush-tips/outline.rst:16
msgid "Question"
msgstr "Pergunta"

#: ../../tutorials/krita-brush-tips/outline.rst:18
msgid "How to make an outline for a single brush stroke using Krita?"
msgstr "Como criar um contorno para uma única passagem do pincel com o Krita?"

#: ../../tutorials/krita-brush-tips/outline.rst:20
msgid ""
"Not really a brush, but what you can do is add a layer style to a layer, by |"
"mouseright| a layer and selecting layer style. Then input the following "
"settings:"
msgstr ""
"Não é de facto um pincel, mas o que poderá fazer é adicionar um estilo de "
"camada à camada propriamente dita, usando o |mouseright| sobre uma camada e "
"seleccionando o estilo da camada. Depois, introduza as seguintes definições:"

#: ../../tutorials/krita-brush-tips/outline.rst:25
msgid ""
"Then, set the main layer to multiply (or add a :ref:`filter_color_to_alpha` "
"filter mask), and paint with white:"
msgstr ""
"Depois, configure a camada principal como multiplicativa (ou adicione uma "
"máscara de filtragem :ref:`filter_color_to_alpha`) e pinte com o branco:"

#: ../../tutorials/krita-brush-tips/outline.rst:30
msgid ""
"(The white thing is the pop-up that you see as you hover over the layer)"
msgstr "(A coisa branca é a área que vê quando passar o cursor sobre a camada)"

#: ../../tutorials/krita-brush-tips/outline.rst:32
msgid "Merge into a empty clear layer after ward to fix all the effects."
msgstr ""
"Faça a junção com uma camada vazia e limpa depois para corrigir todos os "
"efeitos."
